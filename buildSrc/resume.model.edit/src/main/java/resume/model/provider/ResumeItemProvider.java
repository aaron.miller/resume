/**
 */
package resume.model.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import resume.model.ModelFactory;
import resume.model.ModelPackage;
import resume.model.Resume;

/**
 * This is the item provider adapter for a {@link resume.model.Resume} object.
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class ResumeItemProvider
        extends ItemProviderAdapter
        implements
        IEditingDomainItemProvider,
        IStructuredItemContentProvider,
        ITreeItemContentProvider,
        IItemLabelProvider,
        IItemPropertySource {
    /**
     * This constructs an instance from a factory and a notifier. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public ResumeItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);
    }

    /**
     * This returns the property descriptors for the adapted class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
        if (itemPropertyDescriptors == null) {
            super.getPropertyDescriptors(object);

            addTitlePropertyDescriptor(object);
            addCoverLetterPropertyDescriptor(object);
            addWorkExperiencePropertyDescriptor(object);
            addReferencePropertyDescriptor(object);
            addContactPropertyDescriptor(object);
        }
        return itemPropertyDescriptors;
    }

    /**
     * This adds a property descriptor for the Title feature. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void addTitlePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
                createItemPropertyDescriptor(
                        ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                        getResourceLocator(),
                        getString("_UI_Resume_title_feature"),
                        getString(
                                "_UI_PropertyDescriptor_description", "_UI_Resume_title_feature",
                                "_UI_Resume_type"
                        ),
                        ModelPackage.Literals.RESUME__TITLE,
                        true,
                        false,
                        false,
                        ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                        null,
                        null
                )
        );
    }

    /**
     * This adds a property descriptor for the Cover Letter feature. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void addCoverLetterPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
                createItemPropertyDescriptor(
                        ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                        getResourceLocator(),
                        getString("_UI_Resume_coverLetter_feature"),
                        getString(
                                "_UI_PropertyDescriptor_description", "_UI_Resume_coverLetter_feature",
                                "_UI_Resume_type"
                        ),
                        ModelPackage.Literals.RESUME__COVER_LETTER,
                        true,
                        false,
                        false,
                        null,
                        null,
                        null
                )
        );
    }

    /**
     * This adds a property descriptor for the Work Experience feature. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void addWorkExperiencePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
                createItemPropertyDescriptor(
                        ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                        getResourceLocator(),
                        getString("_UI_Resume_workExperience_feature"),
                        getString(
                                "_UI_PropertyDescriptor_description", "_UI_Resume_workExperience_feature",
                                "_UI_Resume_type"
                        ),
                        ModelPackage.Literals.RESUME__WORK_EXPERIENCE,
                        true,
                        false,
                        false,
                        null,
                        null,
                        null
                )
        );
    }

    /**
     * This adds a property descriptor for the Reference feature. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void addReferencePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
                createItemPropertyDescriptor(
                        ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                        getResourceLocator(),
                        getString("_UI_Resume_reference_feature"),
                        getString(
                                "_UI_PropertyDescriptor_description", "_UI_Resume_reference_feature",
                                "_UI_Resume_type"
                        ),
                        ModelPackage.Literals.RESUME__REFERENCE,
                        true,
                        false,
                        false,
                        null,
                        null,
                        null
                )
        );
    }

    /**
     * This adds a property descriptor for the Contact feature. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void addContactPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
                createItemPropertyDescriptor(
                        ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                        getResourceLocator(),
                        getString("_UI_Resume_contact_feature"),
                        getString(
                                "_UI_PropertyDescriptor_description", "_UI_Resume_contact_feature",
                                "_UI_Resume_type"
                        ),
                        ModelPackage.Literals.RESUME__CONTACT,
                        true,
                        false,
                        false,
                        null,
                        null,
                        null
                )
        );
    }

    /**
     * This specifies how to implement {@link #getChildren} and is used to
     * deduce an appropriate feature for an
     * {@link org.eclipse.emf.edit.command.AddCommand},
     * {@link org.eclipse.emf.edit.command.RemoveCommand} or
     * {@link org.eclipse.emf.edit.command.MoveCommand} in
     * {@link #createCommand}. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
        if (childrenFeatures == null) {
            super.getChildrenFeatures(object);
            childrenFeatures.add(ModelPackage.Literals.RESUME__COVER_LETTER);
            childrenFeatures.add(ModelPackage.Literals.RESUME__WORK_EXPERIENCE);
            childrenFeatures.add(ModelPackage.Literals.RESUME__REFERENCE);
            childrenFeatures.add(ModelPackage.Literals.RESUME__CONTACT);
        }
        return childrenFeatures;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EStructuralFeature getChildFeature(Object object, Object child) {
        // Check the type of the specified child object and return the proper
        // feature to use for
        // adding (see {@link AddCommand}) it as a child.

        return super.getChildFeature(object, child);
    }

    /**
     * This returns Resume.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object getImage(Object object) {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/Resume"));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected boolean shouldComposeCreationImage() {
        return true;
    }

    /**
     * This returns the label text for the adapted class. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getText(Object object) {
        String label = ((Resume) object).getTitle();
        return label == null || label.length() == 0 ? getString("_UI_Resume_type")
                : getString("_UI_Resume_type") + " " + label;
    }


    /**
     * This handles model notifications by calling {@link #updateChildren} to
     * update any cached children and by creating a viewer notification, which
     * it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void notifyChanged(Notification notification) {
        updateChildren(notification);

        switch (notification.getFeatureID(Resume.class)) {
        case ModelPackage.RESUME__TITLE:
            fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
            return;
        case ModelPackage.RESUME__COVER_LETTER:
        case ModelPackage.RESUME__WORK_EXPERIENCE:
        case ModelPackage.RESUME__REFERENCE:
        case ModelPackage.RESUME__CONTACT:
            fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
            return;
        }
        super.notifyChanged(notification);
    }

    /**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
     * describing the children that can be created under this object. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);

        newChildDescriptors.add(
                createChildParameter(
                        ModelPackage.Literals.RESUME__COVER_LETTER,
                        ModelFactory.eINSTANCE.createCoverLetter()
                )
        );

        newChildDescriptors.add(
                createChildParameter(
                        ModelPackage.Literals.RESUME__WORK_EXPERIENCE,
                        ModelFactory.eINSTANCE.createWorkExperience()
                )
        );

        newChildDescriptors.add(
                createChildParameter(
                        ModelPackage.Literals.RESUME__REFERENCE,
                        ModelFactory.eINSTANCE.createReference()
                )
        );

        newChildDescriptors.add(
                createChildParameter(
                        ModelPackage.Literals.RESUME__CONTACT,
                        ModelFactory.eINSTANCE.createContactInfo()
                )
        );
    }

    /**
     * Return the resource locator for this item provider's resources. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ResourceLocator getResourceLocator() {
        return ResumeEditPlugin.INSTANCE;
    }

}
