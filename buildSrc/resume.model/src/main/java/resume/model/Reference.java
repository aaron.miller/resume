/**
 */
package resume.model;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object
 * '<em><b>Reference</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link resume.model.Reference#getTitle <em>Title</em>}</li>
 * <li>{@link resume.model.Reference#getItems <em>Items</em>}</li>
 * </ul>
 *
 * @see resume.model.ModelPackage#getReference()
 * @model
 * @generated
 */
public interface Reference extends EObject {
    /**
     * Returns the value of the '<em><b>Title</b></em>' attribute. The default
     * value is <code>"References"</code>. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @return the value of the '<em>Title</em>' attribute.
     * @see #setTitle(String)
     * @see resume.model.ModelPackage#getReference_Title()
     * @model default="References"
     * @generated
     */
    String getTitle();

    /**
     * Sets the value of the '{@link resume.model.Reference#getTitle
     * <em>Title</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Title</em>' attribute.
     * @see #getTitle()
     * @generated
     */
    void setTitle(String value);

    /**
     * Returns the value of the '<em><b>Items</b></em>' containment reference
     * list. The list contents are of type {@link resume.model.ReferenceItem}.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Items</em>' containment reference list.
     * @see resume.model.ModelPackage#getReference_Items()
     * @model containment="true"
     * @generated
     */
    EList<ReferenceItem> getItems();

} // Reference
