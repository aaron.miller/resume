/**
 */
package resume.model.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import resume.model.*;

/**
 * <!-- begin-user-doc --> The <b>Adapter Factory</b> for the model. It provides
 * an adapter <code>createXXX</code> method for each class of the model. <!--
 * end-user-doc -->
 * 
 * @see resume.model.ModelPackage
 * @generated
 */
public class ModelAdapterFactory extends AdapterFactoryImpl {
    /**
     * The cached model package. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static ModelPackage modelPackage;

    /**
     * Creates an instance of the adapter factory. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    public ModelAdapterFactory() {
        if (modelPackage == null) {
            modelPackage = ModelPackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc --> This implementation returns <code>true</code> if
     * the object is either the model's package or is an instance object of the
     * model. <!-- end-user-doc -->
     * 
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType(Object object) {
        if (object == modelPackage) {
            return true;
        }
        if (object instanceof EObject) {
            return ((EObject) object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ModelSwitch<Adapter> modelSwitch =
            new ModelSwitch<Adapter>() {
                @Override
                public Adapter caseResume(Resume object) {
                    return createResumeAdapter();
                }

                @Override
                public Adapter caseCoverLetter(CoverLetter object) {
                    return createCoverLetterAdapter();
                }

                @Override
                public Adapter caseWorkExperience(WorkExperience object) {
                    return createWorkExperienceAdapter();
                }

                @Override
                public Adapter caseReference(Reference object) {
                    return createReferenceAdapter();
                }

                @Override
                public Adapter caseJobPosition(JobPosition object) {
                    return createJobPositionAdapter();
                }

                @Override
                public Adapter caseContactInfo(ContactInfo object) {
                    return createContactInfoAdapter();
                }

                @Override
                public Adapter caseEmployer(Employer object) {
                    return createEmployerAdapter();
                }

                @Override
                public Adapter caseAccomplishment(Accomplishment object) {
                    return createAccomplishmentAdapter();
                }

                @Override
                public Adapter caseSkill(Skill object) {
                    return createSkillAdapter();
                }

                @Override
                public Adapter caseReferenceItem(ReferenceItem object) {
                    return createReferenceItemAdapter();
                }

                @Override
                public Adapter defaultCase(EObject object) {
                    return createEObjectAdapter();
                }
            };

    /**
     * Creates an adapter for the <code>target</code>. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param target
     *            the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject) target);
    }


    /**
     * Creates a new adapter for an object of class '{@link resume.model.Resume
     * <em>Resume</em>}'. <!-- begin-user-doc --> This default implementation
     * returns null so that we can easily ignore cases; it's useful to ignore a
     * case when inheritance will catch all the cases anyway. <!-- end-user-doc
     * -->
     * 
     * @return the new adapter.
     * @see resume.model.Resume
     * @generated
     */
    public Adapter createResumeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link resume.model.CoverLetter <em>Cover Letter</em>}'. <!--
     * begin-user-doc --> This default implementation returns null so that we
     * can easily ignore cases; it's useful to ignore a case when inheritance
     * will catch all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see resume.model.CoverLetter
     * @generated
     */
    public Adapter createCoverLetterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link resume.model.WorkExperience <em>Work Experience</em>}'. <!--
     * begin-user-doc --> This default implementation returns null so that we
     * can easily ignore cases; it's useful to ignore a case when inheritance
     * will catch all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see resume.model.WorkExperience
     * @generated
     */
    public Adapter createWorkExperienceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link resume.model.Reference <em>Reference</em>}'. <!-- begin-user-doc
     * --> This default implementation returns null so that we can easily ignore
     * cases; it's useful to ignore a case when inheritance will catch all the
     * cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see resume.model.Reference
     * @generated
     */
    public Adapter createReferenceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link resume.model.JobPosition <em>Job Position</em>}'. <!--
     * begin-user-doc --> This default implementation returns null so that we
     * can easily ignore cases; it's useful to ignore a case when inheritance
     * will catch all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see resume.model.JobPosition
     * @generated
     */
    public Adapter createJobPositionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link resume.model.ContactInfo <em>Contact Info</em>}'. <!--
     * begin-user-doc --> This default implementation returns null so that we
     * can easily ignore cases; it's useful to ignore a case when inheritance
     * will catch all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see resume.model.ContactInfo
     * @generated
     */
    public Adapter createContactInfoAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link resume.model.Employer <em>Employer</em>}'. <!-- begin-user-doc
     * --> This default implementation returns null so that we can easily ignore
     * cases; it's useful to ignore a case when inheritance will catch all the
     * cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see resume.model.Employer
     * @generated
     */
    public Adapter createEmployerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link resume.model.Accomplishment <em>Accomplishment</em>}'. <!--
     * begin-user-doc --> This default implementation returns null so that we
     * can easily ignore cases; it's useful to ignore a case when inheritance
     * will catch all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see resume.model.Accomplishment
     * @generated
     */
    public Adapter createAccomplishmentAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link resume.model.Skill
     * <em>Skill</em>}'. <!-- begin-user-doc --> This default implementation
     * returns null so that we can easily ignore cases; it's useful to ignore a
     * case when inheritance will catch all the cases anyway. <!-- end-user-doc
     * -->
     * 
     * @return the new adapter.
     * @see resume.model.Skill
     * @generated
     */
    public Adapter createSkillAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link resume.model.ReferenceItem <em>Reference Item</em>}'. <!--
     * begin-user-doc --> This default implementation returns null so that we
     * can easily ignore cases; it's useful to ignore a case when inheritance
     * will catch all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see resume.model.ReferenceItem
     * @generated
     */
    public Adapter createReferenceItemAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for the default case. <!-- begin-user-doc --> This
     * default implementation returns null. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter() {
        return null;
    }

} // ModelAdapterFactory
