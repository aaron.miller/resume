/**
 */
package resume.model.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import resume.model.*;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance
 * hierarchy. It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the
 * inheritance hierarchy until a non-null result is returned, which is the
 * result of the switch. <!-- end-user-doc -->
 * 
 * @see resume.model.ModelPackage
 * @generated
 */
public class ModelSwitch<T> extends Switch<T> {
    /**
     * The cached model package <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static ModelPackage modelPackage;

    /**
     * Creates an instance of the switch. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    public ModelSwitch() {
        if (modelPackage == null) {
            modelPackage = ModelPackage.eINSTANCE;
        }
    }

    /**
     * Checks whether this is a switch for the given package. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param ePackage
     *            the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
    @Override
    protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns
     * a non null result; it yields that result. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @return the first non-null result returned by a <code>caseXXX</code>
     *         call.
     * @generated
     */
    @Override
    protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID) {
        case ModelPackage.RESUME: {
            Resume resume = (Resume) theEObject;
            T result = caseResume(resume);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.COVER_LETTER: {
            CoverLetter coverLetter = (CoverLetter) theEObject;
            T result = caseCoverLetter(coverLetter);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.WORK_EXPERIENCE: {
            WorkExperience workExperience = (WorkExperience) theEObject;
            T result = caseWorkExperience(workExperience);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.REFERENCE: {
            Reference reference = (Reference) theEObject;
            T result = caseReference(reference);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.JOB_POSITION: {
            JobPosition jobPosition = (JobPosition) theEObject;
            T result = caseJobPosition(jobPosition);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.CONTACT_INFO: {
            ContactInfo contactInfo = (ContactInfo) theEObject;
            T result = caseContactInfo(contactInfo);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.EMPLOYER: {
            Employer employer = (Employer) theEObject;
            T result = caseEmployer(employer);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.ACCOMPLISHMENT: {
            Accomplishment accomplishment = (Accomplishment) theEObject;
            T result = caseAccomplishment(accomplishment);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.SKILL: {
            Skill skill = (Skill) theEObject;
            T result = caseSkill(skill);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.REFERENCE_ITEM: {
            ReferenceItem referenceItem = (ReferenceItem) theEObject;
            T result = caseReferenceItem(referenceItem);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        default:
            return defaultCase(theEObject);
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>Resume</em>'. <!-- begin-user-doc --> This implementation returns
     * null; returning a non-null result will terminate the switch. <!--
     * end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>Resume</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseResume(Resume object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>Cover Letter</em>'. <!-- begin-user-doc --> This implementation
     * returns null; returning a non-null result will terminate the switch. <!--
     * end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>Cover Letter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCoverLetter(CoverLetter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Work
     * Experience</em>'. <!-- begin-user-doc --> This implementation returns
     * null; returning a non-null result will terminate the switch. <!--
     * end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Work
     *         Experience</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseWorkExperience(WorkExperience object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>Reference</em>'. <!-- begin-user-doc --> This implementation returns
     * null; returning a non-null result will terminate the switch. <!--
     * end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>Reference</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseReference(Reference object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Job
     * Position</em>'. <!-- begin-user-doc --> This implementation returns null;
     * returning a non-null result will terminate the switch. <!-- end-user-doc
     * -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Job
     *         Position</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseJobPosition(JobPosition object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>Contact Info</em>'. <!-- begin-user-doc --> This implementation
     * returns null; returning a non-null result will terminate the switch. <!--
     * end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>Contact Info</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseContactInfo(ContactInfo object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>Employer</em>'. <!-- begin-user-doc --> This implementation returns
     * null; returning a non-null result will terminate the switch. <!--
     * end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>Employer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEmployer(Employer object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>Accomplishment</em>'. <!-- begin-user-doc --> This implementation
     * returns null; returning a non-null result will terminate the switch. <!--
     * end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>Accomplishment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAccomplishment(Accomplishment object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>Skill</em>'. <!-- begin-user-doc --> This implementation returns
     * null; returning a non-null result will terminate the switch. <!--
     * end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>Skill</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSkill(Skill object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>Reference Item</em>'. <!-- begin-user-doc --> This implementation
     * returns null; returning a non-null result will terminate the switch. <!--
     * end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>Reference Item</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseReferenceItem(ReferenceItem object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>EObject</em>'. <!-- begin-user-doc --> This implementation returns
     * null; returning a non-null result will terminate the switch, but this is
     * the last case anyway. <!-- end-user-doc -->
     * 
     * @param object
     *            the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    @Override
    public T defaultCase(EObject object) {
        return null;
    }

} // ModelSwitch
