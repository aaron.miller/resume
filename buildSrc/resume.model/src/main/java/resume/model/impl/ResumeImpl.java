/**
 */
package resume.model.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import resume.model.ContactInfo;
import resume.model.CoverLetter;
import resume.model.ModelPackage;
import resume.model.Reference;
import resume.model.Resume;
import resume.model.WorkExperience;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Resume</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link resume.model.impl.ResumeImpl#getTitle <em>Title</em>}</li>
 * <li>{@link resume.model.impl.ResumeImpl#getCoverLetter <em>Cover
 * Letter</em>}</li>
 * <li>{@link resume.model.impl.ResumeImpl#getWorkExperience <em>Work
 * Experience</em>}</li>
 * <li>{@link resume.model.impl.ResumeImpl#getReference <em>Reference</em>}</li>
 * <li>{@link resume.model.impl.ResumeImpl#getContact <em>Contact</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResumeImpl extends MinimalEObjectImpl.Container implements Resume {
    /**
     * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected static final String TITLE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected String title = TITLE_EDEFAULT;

    /**
     * The cached value of the '{@link #getCoverLetter() <em>Cover Letter</em>}'
     * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getCoverLetter()
     * @generated
     * @ordered
     */
    protected CoverLetter coverLetter;

    /**
     * The cached value of the '{@link #getWorkExperience() <em>Work
     * Experience</em>}' containment reference. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #getWorkExperience()
     * @generated
     * @ordered
     */
    protected WorkExperience workExperience;

    /**
     * The cached value of the '{@link #getReference() <em>Reference</em>}'
     * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getReference()
     * @generated
     * @ordered
     */
    protected Reference reference;

    /**
     * The cached value of the '{@link #getContact() <em>Contact</em>}'
     * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getContact()
     * @generated
     * @ordered
     */
    protected ContactInfo contact;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ResumeImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ModelPackage.Literals.RESUME;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTitle() {
        return title;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTitle(String newTitle) {
        String oldTitle = title;
        title = newTitle;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RESUME__TITLE, oldTitle, title));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public CoverLetter getCoverLetter() {
        return coverLetter;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetCoverLetter(CoverLetter newCoverLetter, NotificationChain msgs) {
        CoverLetter oldCoverLetter = coverLetter;
        coverLetter = newCoverLetter;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(
                    this, Notification.SET, ModelPackage.RESUME__COVER_LETTER, oldCoverLetter, newCoverLetter
            );
            if (msgs == null)
                msgs = notification;
            else
                msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCoverLetter(CoverLetter newCoverLetter) {
        if (newCoverLetter != coverLetter) {
            NotificationChain msgs = null;
            if (coverLetter != null)
                msgs = ((InternalEObject) coverLetter).eInverseRemove(
                        this, EOPPOSITE_FEATURE_BASE - ModelPackage.RESUME__COVER_LETTER, null, msgs
                );
            if (newCoverLetter != null)
                msgs = ((InternalEObject) newCoverLetter).eInverseAdd(
                        this, EOPPOSITE_FEATURE_BASE - ModelPackage.RESUME__COVER_LETTER, null, msgs
                );
            msgs = basicSetCoverLetter(newCoverLetter, msgs);
            if (msgs != null)
                msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.RESUME__COVER_LETTER, newCoverLetter,
                            newCoverLetter
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public WorkExperience getWorkExperience() {
        return workExperience;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetWorkExperience(WorkExperience newWorkExperience, NotificationChain msgs) {
        WorkExperience oldWorkExperience = workExperience;
        workExperience = newWorkExperience;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(
                    this, Notification.SET, ModelPackage.RESUME__WORK_EXPERIENCE, oldWorkExperience,
                    newWorkExperience
            );
            if (msgs == null)
                msgs = notification;
            else
                msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setWorkExperience(WorkExperience newWorkExperience) {
        if (newWorkExperience != workExperience) {
            NotificationChain msgs = null;
            if (workExperience != null)
                msgs = ((InternalEObject) workExperience).eInverseRemove(
                        this, EOPPOSITE_FEATURE_BASE - ModelPackage.RESUME__WORK_EXPERIENCE, null, msgs
                );
            if (newWorkExperience != null)
                msgs = ((InternalEObject) newWorkExperience).eInverseAdd(
                        this, EOPPOSITE_FEATURE_BASE - ModelPackage.RESUME__WORK_EXPERIENCE, null, msgs
                );
            msgs = basicSetWorkExperience(newWorkExperience, msgs);
            if (msgs != null)
                msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.RESUME__WORK_EXPERIENCE, newWorkExperience,
                            newWorkExperience
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Reference getReference() {
        return reference;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetReference(Reference newReference, NotificationChain msgs) {
        Reference oldReference = reference;
        reference = newReference;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(
                    this, Notification.SET, ModelPackage.RESUME__REFERENCE, oldReference, newReference
            );
            if (msgs == null)
                msgs = notification;
            else
                msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setReference(Reference newReference) {
        if (newReference != reference) {
            NotificationChain msgs = null;
            if (reference != null)
                msgs = ((InternalEObject) reference).eInverseRemove(
                        this, EOPPOSITE_FEATURE_BASE - ModelPackage.RESUME__REFERENCE, null, msgs
                );
            if (newReference != null)
                msgs = ((InternalEObject) newReference)
                        .eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.RESUME__REFERENCE, null, msgs);
            msgs = basicSetReference(newReference, msgs);
            if (msgs != null)
                msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.RESUME__REFERENCE, newReference, newReference
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ContactInfo getContact() {
        if (contact != null && contact.eIsProxy()) {
            InternalEObject oldContact = (InternalEObject) contact;
            contact = (ContactInfo) eResolveProxy(oldContact);
            if (contact != oldContact) {
                if (eNotificationRequired())
                    eNotify(
                            new ENotificationImpl(
                                    this, Notification.RESOLVE, ModelPackage.RESUME__CONTACT, oldContact,
                                    contact
                            )
                    );
            }
        }
        return contact;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public ContactInfo basicGetContact() {
        return contact;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setContact(ContactInfo newContact) {
        ContactInfo oldContact = contact;
        contact = newContact;
        if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.RESUME__CONTACT, oldContact, contact
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
        case ModelPackage.RESUME__COVER_LETTER:
            return basicSetCoverLetter(null, msgs);
        case ModelPackage.RESUME__WORK_EXPERIENCE:
            return basicSetWorkExperience(null, msgs);
        case ModelPackage.RESUME__REFERENCE:
            return basicSetReference(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
        case ModelPackage.RESUME__TITLE:
            return getTitle();
        case ModelPackage.RESUME__COVER_LETTER:
            return getCoverLetter();
        case ModelPackage.RESUME__WORK_EXPERIENCE:
            return getWorkExperience();
        case ModelPackage.RESUME__REFERENCE:
            return getReference();
        case ModelPackage.RESUME__CONTACT:
            if (resolve)
                return getContact();
            return basicGetContact();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
        case ModelPackage.RESUME__TITLE:
            setTitle((String) newValue);
            return;
        case ModelPackage.RESUME__COVER_LETTER:
            setCoverLetter((CoverLetter) newValue);
            return;
        case ModelPackage.RESUME__WORK_EXPERIENCE:
            setWorkExperience((WorkExperience) newValue);
            return;
        case ModelPackage.RESUME__REFERENCE:
            setReference((Reference) newValue);
            return;
        case ModelPackage.RESUME__CONTACT:
            setContact((ContactInfo) newValue);
            return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
        case ModelPackage.RESUME__TITLE:
            setTitle(TITLE_EDEFAULT);
            return;
        case ModelPackage.RESUME__COVER_LETTER:
            setCoverLetter((CoverLetter) null);
            return;
        case ModelPackage.RESUME__WORK_EXPERIENCE:
            setWorkExperience((WorkExperience) null);
            return;
        case ModelPackage.RESUME__REFERENCE:
            setReference((Reference) null);
            return;
        case ModelPackage.RESUME__CONTACT:
            setContact((ContactInfo) null);
            return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
        case ModelPackage.RESUME__TITLE:
            return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
        case ModelPackage.RESUME__COVER_LETTER:
            return coverLetter != null;
        case ModelPackage.RESUME__WORK_EXPERIENCE:
            return workExperience != null;
        case ModelPackage.RESUME__REFERENCE:
            return reference != null;
        case ModelPackage.RESUME__CONTACT:
            return contact != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy())
            return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (title: ");
        result.append(title);
        result.append(')');
        return result.toString();
    }

} // ResumeImpl
