/**
 */
package resume.model.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import resume.model.ContactInfo;
import resume.model.Employer;
import resume.model.ModelPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Employer</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link resume.model.impl.EmployerImpl#getName <em>Name</em>}</li>
 * <li>{@link resume.model.impl.EmployerImpl#getContact <em>Contact</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EmployerImpl extends MinimalEObjectImpl.Container implements Employer {
    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * The cached value of the '{@link #getContact() <em>Contact</em>}'
     * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getContact()
     * @generated
     * @ordered
     */
    protected ContactInfo contact;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected EmployerImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ModelPackage.Literals.EMPLOYER;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.EMPLOYER__NAME, oldName, name));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ContactInfo getContact() {
        if (contact != null && contact.eIsProxy()) {
            InternalEObject oldContact = (InternalEObject) contact;
            contact = (ContactInfo) eResolveProxy(oldContact);
            if (contact != oldContact) {
                if (eNotificationRequired())
                    eNotify(
                            new ENotificationImpl(
                                    this, Notification.RESOLVE, ModelPackage.EMPLOYER__CONTACT, oldContact,
                                    contact
                            )
                    );
            }
        }
        return contact;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public ContactInfo basicGetContact() {
        return contact;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setContact(ContactInfo newContact) {
        ContactInfo oldContact = contact;
        contact = newContact;
        if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.EMPLOYER__CONTACT, oldContact, contact
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
        case ModelPackage.EMPLOYER__NAME:
            return getName();
        case ModelPackage.EMPLOYER__CONTACT:
            if (resolve)
                return getContact();
            return basicGetContact();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
        case ModelPackage.EMPLOYER__NAME:
            setName((String) newValue);
            return;
        case ModelPackage.EMPLOYER__CONTACT:
            setContact((ContactInfo) newValue);
            return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
        case ModelPackage.EMPLOYER__NAME:
            setName(NAME_EDEFAULT);
            return;
        case ModelPackage.EMPLOYER__CONTACT:
            setContact((ContactInfo) null);
            return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
        case ModelPackage.EMPLOYER__NAME:
            return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
        case ModelPackage.EMPLOYER__CONTACT:
            return contact != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy())
            return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(')');
        return result.toString();
    }

} // EmployerImpl
