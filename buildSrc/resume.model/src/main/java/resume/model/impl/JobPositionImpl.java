/**
 */
package resume.model.impl;

import java.util.Collection;
import java.util.Date;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import resume.model.Accomplishment;
import resume.model.Employer;
import resume.model.JobPosition;
import resume.model.ModelPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Job
 * Position</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link resume.model.impl.JobPositionImpl#getStartDate <em>Start
 * Date</em>}</li>
 * <li>{@link resume.model.impl.JobPositionImpl#getEndDate <em>End
 * Date</em>}</li>
 * <li>{@link resume.model.impl.JobPositionImpl#getTitle <em>Title</em>}</li>
 * <li>{@link resume.model.impl.JobPositionImpl#getEmployer
 * <em>Employer</em>}</li>
 * <li>{@link resume.model.impl.JobPositionImpl#getDescription
 * <em>Description</em>}</li>
 * <li>{@link resume.model.impl.JobPositionImpl#getAccomplishments
 * <em>Accomplishments</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JobPositionImpl extends MinimalEObjectImpl.Container implements JobPosition {
    /**
     * The default value of the '{@link #getStartDate() <em>Start Date</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getStartDate()
     * @generated
     * @ordered
     */
    protected static final Date START_DATE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getStartDate() <em>Start Date</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getStartDate()
     * @generated
     * @ordered
     */
    protected Date startDate = START_DATE_EDEFAULT;

    /**
     * The default value of the '{@link #getEndDate() <em>End Date</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getEndDate()
     * @generated
     * @ordered
     */
    protected static final Date END_DATE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getEndDate() <em>End Date</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getEndDate()
     * @generated
     * @ordered
     */
    protected Date endDate = END_DATE_EDEFAULT;

    /**
     * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected static final String TITLE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected String title = TITLE_EDEFAULT;

    /**
     * The cached value of the '{@link #getEmployer() <em>Employer</em>}'
     * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getEmployer()
     * @generated
     * @ordered
     */
    protected Employer employer;

    /**
     * The default value of the '{@link #getDescription() <em>Description</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected static final String DESCRIPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getDescription() <em>Description</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected String description = DESCRIPTION_EDEFAULT;

    /**
     * The cached value of the '{@link #getAccomplishments()
     * <em>Accomplishments</em>}' containment reference list. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getAccomplishments()
     * @generated
     * @ordered
     */
    protected EList<Accomplishment> accomplishments;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected JobPositionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ModelPackage.Literals.JOB_POSITION;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Employer getEmployer() {
        if (employer != null && employer.eIsProxy()) {
            InternalEObject oldEmployer = (InternalEObject) employer;
            employer = (Employer) eResolveProxy(oldEmployer);
            if (employer != oldEmployer) {
                if (eNotificationRequired())
                    eNotify(
                            new ENotificationImpl(
                                    this, Notification.RESOLVE, ModelPackage.JOB_POSITION__EMPLOYER,
                                    oldEmployer, employer
                            )
                    );
            }
        }
        return employer;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public Employer basicGetEmployer() {
        return employer;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setEmployer(Employer newEmployer) {
        Employer oldEmployer = employer;
        employer = newEmployer;
        if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.JOB_POSITION__EMPLOYER, oldEmployer, employer
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Date getStartDate() {
        return startDate;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStartDate(Date newStartDate) {
        Date oldStartDate = startDate;
        startDate = newStartDate;
        if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.JOB_POSITION__START_DATE, oldStartDate,
                            startDate
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Date getEndDate() {
        return endDate;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setEndDate(Date newEndDate) {
        Date oldEndDate = endDate;
        endDate = newEndDate;
        if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.JOB_POSITION__END_DATE, oldEndDate, endDate
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTitle() {
        return title;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTitle(String newTitle) {
        String oldTitle = title;
        title = newTitle;
        if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.JOB_POSITION__TITLE, oldTitle, title
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDescription(String newDescription) {
        String oldDescription = description;
        description = newDescription;
        if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.JOB_POSITION__DESCRIPTION, oldDescription,
                            description
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Accomplishment> getAccomplishments() {
        if (accomplishments == null) {
            accomplishments = new EObjectContainmentEList<Accomplishment>(
                    Accomplishment.class, this, ModelPackage.JOB_POSITION__ACCOMPLISHMENTS
            );
        }
        return accomplishments;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
        case ModelPackage.JOB_POSITION__ACCOMPLISHMENTS:
            return ((InternalEList<?>) getAccomplishments()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
        case ModelPackage.JOB_POSITION__START_DATE:
            return getStartDate();
        case ModelPackage.JOB_POSITION__END_DATE:
            return getEndDate();
        case ModelPackage.JOB_POSITION__TITLE:
            return getTitle();
        case ModelPackage.JOB_POSITION__EMPLOYER:
            if (resolve)
                return getEmployer();
            return basicGetEmployer();
        case ModelPackage.JOB_POSITION__DESCRIPTION:
            return getDescription();
        case ModelPackage.JOB_POSITION__ACCOMPLISHMENTS:
            return getAccomplishments();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
        case ModelPackage.JOB_POSITION__START_DATE:
            setStartDate((Date) newValue);
            return;
        case ModelPackage.JOB_POSITION__END_DATE:
            setEndDate((Date) newValue);
            return;
        case ModelPackage.JOB_POSITION__TITLE:
            setTitle((String) newValue);
            return;
        case ModelPackage.JOB_POSITION__EMPLOYER:
            setEmployer((Employer) newValue);
            return;
        case ModelPackage.JOB_POSITION__DESCRIPTION:
            setDescription((String) newValue);
            return;
        case ModelPackage.JOB_POSITION__ACCOMPLISHMENTS:
            getAccomplishments().clear();
            getAccomplishments().addAll((Collection<? extends Accomplishment>) newValue);
            return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
        case ModelPackage.JOB_POSITION__START_DATE:
            setStartDate(START_DATE_EDEFAULT);
            return;
        case ModelPackage.JOB_POSITION__END_DATE:
            setEndDate(END_DATE_EDEFAULT);
            return;
        case ModelPackage.JOB_POSITION__TITLE:
            setTitle(TITLE_EDEFAULT);
            return;
        case ModelPackage.JOB_POSITION__EMPLOYER:
            setEmployer((Employer) null);
            return;
        case ModelPackage.JOB_POSITION__DESCRIPTION:
            setDescription(DESCRIPTION_EDEFAULT);
            return;
        case ModelPackage.JOB_POSITION__ACCOMPLISHMENTS:
            getAccomplishments().clear();
            return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
        case ModelPackage.JOB_POSITION__START_DATE:
            return START_DATE_EDEFAULT == null ? startDate != null : !START_DATE_EDEFAULT.equals(startDate);
        case ModelPackage.JOB_POSITION__END_DATE:
            return END_DATE_EDEFAULT == null ? endDate != null : !END_DATE_EDEFAULT.equals(endDate);
        case ModelPackage.JOB_POSITION__TITLE:
            return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
        case ModelPackage.JOB_POSITION__EMPLOYER:
            return employer != null;
        case ModelPackage.JOB_POSITION__DESCRIPTION:
            return DESCRIPTION_EDEFAULT == null ? description != null
                    : !DESCRIPTION_EDEFAULT.equals(description);
        case ModelPackage.JOB_POSITION__ACCOMPLISHMENTS:
            return accomplishments != null && !accomplishments.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy())
            return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (startDate: ");
        result.append(startDate);
        result.append(", endDate: ");
        result.append(endDate);
        result.append(", title: ");
        result.append(title);
        result.append(", description: ");
        result.append(description);
        result.append(')');
        return result.toString();
    }

} // JobPositionImpl
