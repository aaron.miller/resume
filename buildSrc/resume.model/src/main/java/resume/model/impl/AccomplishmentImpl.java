/**
 */
package resume.model.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import resume.model.Accomplishment;
import resume.model.ModelPackage;
import resume.model.Skill;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Accomplishment</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link resume.model.impl.AccomplishmentImpl#getTitle <em>Title</em>}</li>
 * <li>{@link resume.model.impl.AccomplishmentImpl#getDescription
 * <em>Description</em>}</li>
 * <li>{@link resume.model.impl.AccomplishmentImpl#getSkills
 * <em>Skills</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AccomplishmentImpl extends MinimalEObjectImpl.Container implements Accomplishment {
    /**
     * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected static final String TITLE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected String title = TITLE_EDEFAULT;

    /**
     * The default value of the '{@link #getDescription() <em>Description</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected static final String DESCRIPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getDescription() <em>Description</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected String description = DESCRIPTION_EDEFAULT;

    /**
     * The cached value of the '{@link #getSkills() <em>Skills</em>}' reference
     * list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getSkills()
     * @generated
     * @ordered
     */
    protected EList<Skill> skills;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected AccomplishmentImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ModelPackage.Literals.ACCOMPLISHMENT;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTitle() {
        return title;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTitle(String newTitle) {
        String oldTitle = title;
        title = newTitle;
        if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.ACCOMPLISHMENT__TITLE, oldTitle, title
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Skill> getSkills() {
        if (skills == null) {
            skills = new EObjectResolvingEList<Skill>(Skill.class, this, ModelPackage.ACCOMPLISHMENT__SKILLS);
        }
        return skills;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDescription(String newDescription) {
        String oldDescription = description;
        description = newDescription;
        if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.ACCOMPLISHMENT__DESCRIPTION, oldDescription,
                            description
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
        case ModelPackage.ACCOMPLISHMENT__TITLE:
            return getTitle();
        case ModelPackage.ACCOMPLISHMENT__DESCRIPTION:
            return getDescription();
        case ModelPackage.ACCOMPLISHMENT__SKILLS:
            return getSkills();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
        case ModelPackage.ACCOMPLISHMENT__TITLE:
            setTitle((String) newValue);
            return;
        case ModelPackage.ACCOMPLISHMENT__DESCRIPTION:
            setDescription((String) newValue);
            return;
        case ModelPackage.ACCOMPLISHMENT__SKILLS:
            getSkills().clear();
            getSkills().addAll((Collection<? extends Skill>) newValue);
            return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
        case ModelPackage.ACCOMPLISHMENT__TITLE:
            setTitle(TITLE_EDEFAULT);
            return;
        case ModelPackage.ACCOMPLISHMENT__DESCRIPTION:
            setDescription(DESCRIPTION_EDEFAULT);
            return;
        case ModelPackage.ACCOMPLISHMENT__SKILLS:
            getSkills().clear();
            return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
        case ModelPackage.ACCOMPLISHMENT__TITLE:
            return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
        case ModelPackage.ACCOMPLISHMENT__DESCRIPTION:
            return DESCRIPTION_EDEFAULT == null ? description != null
                    : !DESCRIPTION_EDEFAULT.equals(description);
        case ModelPackage.ACCOMPLISHMENT__SKILLS:
            return skills != null && !skills.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy())
            return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (title: ");
        result.append(title);
        result.append(", description: ");
        result.append(description);
        result.append(')');
        return result.toString();
    }

} // AccomplishmentImpl
