/**
 */
package resume.model.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import resume.model.Accomplishment;
import resume.model.ContactInfo;
import resume.model.CoverLetter;
import resume.model.Employer;
import resume.model.JobPosition;
import resume.model.ModelFactory;
import resume.model.ModelPackage;
import resume.model.Reference;
import resume.model.ReferenceItem;
import resume.model.Resume;
import resume.model.Skill;
import resume.model.WorkExperience;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class ModelPackageImpl extends EPackageImpl implements ModelPackage {
    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass resumeEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass coverLetterEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass workExperienceEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass referenceEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass jobPositionEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass contactInfoEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass employerEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass accomplishmentEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass skillEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass referenceItemEClass = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
     * package package URI value.
     * <p>
     * Note: the correct way to create the package is via the static factory
     * method {@link #init init()}, which also performs initialization of the
     * package, or returns the registered package, if one already exists. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see resume.model.ModelPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private ModelPackageImpl() {
        super(eNS_URI, ModelFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model,
     * and for any others upon which it depends.
     *
     * <p>
     * This method is used to initialize {@link ModelPackage#eINSTANCE} when
     * that field is accessed. Clients should not invoke it directly. Instead,
     * they should simply access that field to obtain the package. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static ModelPackage init() {
        if (isInited)
            return (ModelPackage) EPackage.Registry.INSTANCE.getEPackage(ModelPackage.eNS_URI);

        // Obtain or create and register package
        Object registeredModelPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
        ModelPackageImpl theModelPackage =
                registeredModelPackage instanceof ModelPackageImpl ? (ModelPackageImpl) registeredModelPackage
                        : new ModelPackageImpl();

        isInited = true;

        // Create package meta-data objects
        theModelPackage.createPackageContents();

        // Initialize created meta-data
        theModelPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theModelPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(ModelPackage.eNS_URI, theModelPackage);
        return theModelPackage;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getResume() {
        return resumeEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getResume_Title() {
        return (EAttribute) resumeEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getResume_CoverLetter() {
        return (EReference) resumeEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getResume_WorkExperience() {
        return (EReference) resumeEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getResume_Reference() {
        return (EReference) resumeEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getResume_Contact() {
        return (EReference) resumeEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getCoverLetter() {
        return coverLetterEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCoverLetter_Title() {
        return (EAttribute) coverLetterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCoverLetter_Content() {
        return (EAttribute) coverLetterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getWorkExperience() {
        return workExperienceEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getWorkExperience_Title() {
        return (EAttribute) workExperienceEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getWorkExperience_Positions() {
        return (EReference) workExperienceEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getReference() {
        return referenceEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getReference_Title() {
        return (EAttribute) referenceEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getReference_Items() {
        return (EReference) referenceEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getJobPosition() {
        return jobPositionEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getJobPosition_Employer() {
        return (EReference) jobPositionEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getJobPosition_StartDate() {
        return (EAttribute) jobPositionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getJobPosition_EndDate() {
        return (EAttribute) jobPositionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getJobPosition_Title() {
        return (EAttribute) jobPositionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getJobPosition_Description() {
        return (EAttribute) jobPositionEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getJobPosition_Accomplishments() {
        return (EReference) jobPositionEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getContactInfo() {
        return contactInfoEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getContactInfo_Name() {
        return (EAttribute) contactInfoEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getContactInfo_Email() {
        return (EAttribute) contactInfoEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getContactInfo_Phone() {
        return (EAttribute) contactInfoEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getContactInfo_Address() {
        return (EAttribute) contactInfoEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getContactInfo_Website() {
        return (EAttribute) contactInfoEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getEmployer() {
        return employerEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getEmployer_Name() {
        return (EAttribute) employerEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getEmployer_Contact() {
        return (EReference) employerEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getAccomplishment() {
        return accomplishmentEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAccomplishment_Title() {
        return (EAttribute) accomplishmentEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getAccomplishment_Skills() {
        return (EReference) accomplishmentEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAccomplishment_Description() {
        return (EAttribute) accomplishmentEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getSkill() {
        return skillEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSkill_Name() {
        return (EAttribute) skillEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getReferenceItem() {
        return referenceItemEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getReferenceItem_Description() {
        return (EAttribute) referenceItemEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getReferenceItem_Uri() {
        return (EAttribute) referenceItemEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ModelFactory getModelFactory() {
        return (ModelFactory) getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is guarded to
     * have no affect on any invocation but its first. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void createPackageContents() {
        if (isCreated)
            return;
        isCreated = true;

        // Create classes and their features
        resumeEClass = createEClass(RESUME);
        createEAttribute(resumeEClass, RESUME__TITLE);
        createEReference(resumeEClass, RESUME__COVER_LETTER);
        createEReference(resumeEClass, RESUME__WORK_EXPERIENCE);
        createEReference(resumeEClass, RESUME__REFERENCE);
        createEReference(resumeEClass, RESUME__CONTACT);

        coverLetterEClass = createEClass(COVER_LETTER);
        createEAttribute(coverLetterEClass, COVER_LETTER__TITLE);
        createEAttribute(coverLetterEClass, COVER_LETTER__CONTENT);

        workExperienceEClass = createEClass(WORK_EXPERIENCE);
        createEAttribute(workExperienceEClass, WORK_EXPERIENCE__TITLE);
        createEReference(workExperienceEClass, WORK_EXPERIENCE__POSITIONS);

        referenceEClass = createEClass(REFERENCE);
        createEAttribute(referenceEClass, REFERENCE__TITLE);
        createEReference(referenceEClass, REFERENCE__ITEMS);

        jobPositionEClass = createEClass(JOB_POSITION);
        createEAttribute(jobPositionEClass, JOB_POSITION__START_DATE);
        createEAttribute(jobPositionEClass, JOB_POSITION__END_DATE);
        createEAttribute(jobPositionEClass, JOB_POSITION__TITLE);
        createEReference(jobPositionEClass, JOB_POSITION__EMPLOYER);
        createEAttribute(jobPositionEClass, JOB_POSITION__DESCRIPTION);
        createEReference(jobPositionEClass, JOB_POSITION__ACCOMPLISHMENTS);

        contactInfoEClass = createEClass(CONTACT_INFO);
        createEAttribute(contactInfoEClass, CONTACT_INFO__NAME);
        createEAttribute(contactInfoEClass, CONTACT_INFO__EMAIL);
        createEAttribute(contactInfoEClass, CONTACT_INFO__PHONE);
        createEAttribute(contactInfoEClass, CONTACT_INFO__ADDRESS);
        createEAttribute(contactInfoEClass, CONTACT_INFO__WEBSITE);

        employerEClass = createEClass(EMPLOYER);
        createEAttribute(employerEClass, EMPLOYER__NAME);
        createEReference(employerEClass, EMPLOYER__CONTACT);

        accomplishmentEClass = createEClass(ACCOMPLISHMENT);
        createEAttribute(accomplishmentEClass, ACCOMPLISHMENT__TITLE);
        createEAttribute(accomplishmentEClass, ACCOMPLISHMENT__DESCRIPTION);
        createEReference(accomplishmentEClass, ACCOMPLISHMENT__SKILLS);

        skillEClass = createEClass(SKILL);
        createEAttribute(skillEClass, SKILL__NAME);

        referenceItemEClass = createEClass(REFERENCE_ITEM);
        createEAttribute(referenceItemEClass, REFERENCE_ITEM__DESCRIPTION);
        createEAttribute(referenceItemEClass, REFERENCE_ITEM__URI);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This
     * method is guarded to have no affect on any invocation but its first. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized)
            return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes

        // Initialize classes, features, and operations; add parameters
        initEClass(
                resumeEClass, Resume.class, "Resume", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS
        );
        initEAttribute(
                getResume_Title(), ecorePackage.getEString(), "title", null, 0, 1, Resume.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );
        initEReference(
                getResume_CoverLetter(), this.getCoverLetter(), null, "coverLetter", null, 1, 1, Resume.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );
        initEReference(
                getResume_WorkExperience(), this.getWorkExperience(), null, "workExperience", null, 1, 1,
                Resume.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
                !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );
        initEReference(
                getResume_Reference(), this.getReference(), null, "reference", null, 1, 1, Resume.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );
        initEReference(
                getResume_Contact(), this.getContactInfo(), null, "contact", null, 1, 1, Resume.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );

        initEClass(
                coverLetterEClass, CoverLetter.class, "CoverLetter", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS
        );
        initEAttribute(
                getCoverLetter_Title(), ecorePackage.getEString(), "title", "Cover Letter", 0, 1,
                CoverLetter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );
        initEAttribute(
                getCoverLetter_Content(), ecorePackage.getEString(), "content", null, 0, 1, CoverLetter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED
        );

        initEClass(
                workExperienceEClass, WorkExperience.class, "WorkExperience", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS
        );
        initEAttribute(
                getWorkExperience_Title(), ecorePackage.getEString(), "title", "Work Experience", 0, 1,
                WorkExperience.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );
        initEReference(
                getWorkExperience_Positions(), this.getJobPosition(), null, "positions", null, 0, -1,
                WorkExperience.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
                !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );

        initEClass(
                referenceEClass, Reference.class, "Reference", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS
        );
        initEAttribute(
                getReference_Title(), ecorePackage.getEString(), "title", "References", 0, 1, Reference.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED
        );
        initEReference(
                getReference_Items(), this.getReferenceItem(), null, "items", null, 0, -1, Reference.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );

        initEClass(
                jobPositionEClass, JobPosition.class, "JobPosition", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS
        );
        initEAttribute(
                getJobPosition_StartDate(), ecorePackage.getEDate(), "startDate", null, 0, 1, JobPosition.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED
        );
        initEAttribute(
                getJobPosition_EndDate(), ecorePackage.getEDate(), "endDate", null, 0, 1, JobPosition.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED
        );
        initEAttribute(
                getJobPosition_Title(), ecorePackage.getEString(), "title", null, 0, 1, JobPosition.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED
        );
        initEReference(
                getJobPosition_Employer(), this.getEmployer(), null, "employer", null, 1, 1, JobPosition.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );
        initEAttribute(
                getJobPosition_Description(), ecorePackage.getEString(), "description", null, 0, 1,
                JobPosition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );
        initEReference(
                getJobPosition_Accomplishments(), this.getAccomplishment(), null, "accomplishments", null, 0,
                -1, JobPosition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
                !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );

        initEClass(
                contactInfoEClass, ContactInfo.class, "ContactInfo", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS
        );
        initEAttribute(
                getContactInfo_Name(), ecorePackage.getEString(), "name", null, 0, 1, ContactInfo.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED
        );
        initEAttribute(
                getContactInfo_Email(), ecorePackage.getEString(), "email", null, 0, 1, ContactInfo.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED
        );
        initEAttribute(
                getContactInfo_Phone(), ecorePackage.getEString(), "phone", null, 0, 1, ContactInfo.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED
        );
        initEAttribute(
                getContactInfo_Address(), ecorePackage.getEString(), "address", null, 0, 1, ContactInfo.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED
        );
        initEAttribute(
                getContactInfo_Website(), ecorePackage.getEString(), "website", null, 0, 1, ContactInfo.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED
        );

        initEClass(
                employerEClass, Employer.class, "Employer", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS
        );
        initEAttribute(
                getEmployer_Name(), ecorePackage.getEString(), "name", null, 0, 1, Employer.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED
        );
        initEReference(
                getEmployer_Contact(), this.getContactInfo(), null, "contact", null, 0, 1, Employer.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );

        initEClass(
                accomplishmentEClass, Accomplishment.class, "Accomplishment", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS
        );
        initEAttribute(
                getAccomplishment_Title(), ecorePackage.getEString(), "title", null, 0, 1, Accomplishment.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED
        );
        initEAttribute(
                getAccomplishment_Description(), ecorePackage.getEString(), "description", null, 0, 1,
                Accomplishment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );
        initEReference(
                getAccomplishment_Skills(), this.getSkill(), null, "skills", null, 0, -1, Accomplishment.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );

        initEClass(skillEClass, Skill.class, "Skill", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(
                getSkill_Name(), ecorePackage.getEString(), "name", null, 0, 1, Skill.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );

        initEClass(
                referenceItemEClass, ReferenceItem.class, "ReferenceItem", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS
        );
        initEAttribute(
                getReferenceItem_Description(), ecorePackage.getEString(), "description", null, 0, 1,
                ReferenceItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED
        );
        initEAttribute(
                getReferenceItem_Uri(), ecorePackage.getEString(), "uri", null, 0, 1, ReferenceItem.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED
        );

        // Create resource
        createResource(eNS_URI);
    }

} // ModelPackageImpl
