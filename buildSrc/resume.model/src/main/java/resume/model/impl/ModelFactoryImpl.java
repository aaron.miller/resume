/**
 */
package resume.model.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import resume.model.*;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class ModelFactoryImpl extends EFactoryImpl implements ModelFactory {
    /**
     * Creates the default factory implementation. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    public static ModelFactory init() {
        try {
            ModelFactory theModelFactory =
                    (ModelFactory) EPackage.Registry.INSTANCE.getEFactory(ModelPackage.eNS_URI);
            if (theModelFactory != null) {
                return theModelFactory;
            }
        }
        catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new ModelFactoryImpl();
    }

    /**
     * Creates an instance of the factory. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    public ModelFactoryImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
        case ModelPackage.RESUME:
            return createResume();
        case ModelPackage.COVER_LETTER:
            return createCoverLetter();
        case ModelPackage.WORK_EXPERIENCE:
            return createWorkExperience();
        case ModelPackage.REFERENCE:
            return createReference();
        case ModelPackage.JOB_POSITION:
            return createJobPosition();
        case ModelPackage.CONTACT_INFO:
            return createContactInfo();
        case ModelPackage.EMPLOYER:
            return createEmployer();
        case ModelPackage.ACCOMPLISHMENT:
            return createAccomplishment();
        case ModelPackage.SKILL:
            return createSkill();
        case ModelPackage.REFERENCE_ITEM:
            return createReferenceItem();
        default:
            throw new IllegalArgumentException(
                    "The class '" + eClass.getName() + "' is not a valid classifier"
            );
        }
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Resume createResume() {
        ResumeImpl resume = new ResumeImpl();
        return resume;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public CoverLetter createCoverLetter() {
        CoverLetterImpl coverLetter = new CoverLetterImpl();
        return coverLetter;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public WorkExperience createWorkExperience() {
        WorkExperienceImpl workExperience = new WorkExperienceImpl();
        return workExperience;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Reference createReference() {
        ReferenceImpl reference = new ReferenceImpl();
        return reference;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public JobPosition createJobPosition() {
        JobPositionImpl jobPosition = new JobPositionImpl();
        return jobPosition;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ContactInfo createContactInfo() {
        ContactInfoImpl contactInfo = new ContactInfoImpl();
        return contactInfo;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Employer createEmployer() {
        EmployerImpl employer = new EmployerImpl();
        return employer;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Accomplishment createAccomplishment() {
        AccomplishmentImpl accomplishment = new AccomplishmentImpl();
        return accomplishment;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Skill createSkill() {
        SkillImpl skill = new SkillImpl();
        return skill;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ReferenceItem createReferenceItem() {
        ReferenceItemImpl referenceItem = new ReferenceItemImpl();
        return referenceItem;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ModelPackage getModelPackage() {
        return (ModelPackage) getEPackage();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @deprecated
     * @generated
     */
    @Deprecated
    public static ModelPackage getPackage() {
        return ModelPackage.eINSTANCE;
    }

} // ModelFactoryImpl
