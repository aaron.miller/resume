/**
 */
package resume.model.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import resume.model.ContactInfo;
import resume.model.ModelPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Contact
 * Info</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link resume.model.impl.ContactInfoImpl#getName <em>Name</em>}</li>
 * <li>{@link resume.model.impl.ContactInfoImpl#getEmail <em>Email</em>}</li>
 * <li>{@link resume.model.impl.ContactInfoImpl#getPhone <em>Phone</em>}</li>
 * <li>{@link resume.model.impl.ContactInfoImpl#getAddress
 * <em>Address</em>}</li>
 * <li>{@link resume.model.impl.ContactInfoImpl#getWebsite
 * <em>Website</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ContactInfoImpl extends MinimalEObjectImpl.Container implements ContactInfo {
    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * The default value of the '{@link #getEmail() <em>Email</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getEmail()
     * @generated
     * @ordered
     */
    protected static final String EMAIL_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getEmail() <em>Email</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getEmail()
     * @generated
     * @ordered
     */
    protected String email = EMAIL_EDEFAULT;

    /**
     * The default value of the '{@link #getPhone() <em>Phone</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getPhone()
     * @generated
     * @ordered
     */
    protected static final String PHONE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getPhone() <em>Phone</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getPhone()
     * @generated
     * @ordered
     */
    protected String phone = PHONE_EDEFAULT;

    /**
     * The default value of the '{@link #getAddress() <em>Address</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getAddress()
     * @generated
     * @ordered
     */
    protected static final String ADDRESS_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getAddress() <em>Address</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getAddress()
     * @generated
     * @ordered
     */
    protected String address = ADDRESS_EDEFAULT;

    /**
     * The default value of the '{@link #getWebsite() <em>Website</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getWebsite()
     * @generated
     * @ordered
     */
    protected static final String WEBSITE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getWebsite() <em>Website</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getWebsite()
     * @generated
     * @ordered
     */
    protected String website = WEBSITE_EDEFAULT;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ContactInfoImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ModelPackage.Literals.CONTACT_INFO;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(this, Notification.SET, ModelPackage.CONTACT_INFO__NAME, oldName, name)
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getEmail() {
        return email;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setEmail(String newEmail) {
        String oldEmail = email;
        email = newEmail;
        if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.CONTACT_INFO__EMAIL, oldEmail, email
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getPhone() {
        return phone;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setPhone(String newPhone) {
        String oldPhone = phone;
        phone = newPhone;
        if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.CONTACT_INFO__PHONE, oldPhone, phone
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getAddress() {
        return address;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setAddress(String newAddress) {
        String oldAddress = address;
        address = newAddress;
        if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.CONTACT_INFO__ADDRESS, oldAddress, address
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getWebsite() {
        return website;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setWebsite(String newWebsite) {
        String oldWebsite = website;
        website = newWebsite;
        if (eNotificationRequired())
            eNotify(
                    new ENotificationImpl(
                            this, Notification.SET, ModelPackage.CONTACT_INFO__WEBSITE, oldWebsite, website
                    )
            );
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
        case ModelPackage.CONTACT_INFO__NAME:
            return getName();
        case ModelPackage.CONTACT_INFO__EMAIL:
            return getEmail();
        case ModelPackage.CONTACT_INFO__PHONE:
            return getPhone();
        case ModelPackage.CONTACT_INFO__ADDRESS:
            return getAddress();
        case ModelPackage.CONTACT_INFO__WEBSITE:
            return getWebsite();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
        case ModelPackage.CONTACT_INFO__NAME:
            setName((String) newValue);
            return;
        case ModelPackage.CONTACT_INFO__EMAIL:
            setEmail((String) newValue);
            return;
        case ModelPackage.CONTACT_INFO__PHONE:
            setPhone((String) newValue);
            return;
        case ModelPackage.CONTACT_INFO__ADDRESS:
            setAddress((String) newValue);
            return;
        case ModelPackage.CONTACT_INFO__WEBSITE:
            setWebsite((String) newValue);
            return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
        case ModelPackage.CONTACT_INFO__NAME:
            setName(NAME_EDEFAULT);
            return;
        case ModelPackage.CONTACT_INFO__EMAIL:
            setEmail(EMAIL_EDEFAULT);
            return;
        case ModelPackage.CONTACT_INFO__PHONE:
            setPhone(PHONE_EDEFAULT);
            return;
        case ModelPackage.CONTACT_INFO__ADDRESS:
            setAddress(ADDRESS_EDEFAULT);
            return;
        case ModelPackage.CONTACT_INFO__WEBSITE:
            setWebsite(WEBSITE_EDEFAULT);
            return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
        case ModelPackage.CONTACT_INFO__NAME:
            return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
        case ModelPackage.CONTACT_INFO__EMAIL:
            return EMAIL_EDEFAULT == null ? email != null : !EMAIL_EDEFAULT.equals(email);
        case ModelPackage.CONTACT_INFO__PHONE:
            return PHONE_EDEFAULT == null ? phone != null : !PHONE_EDEFAULT.equals(phone);
        case ModelPackage.CONTACT_INFO__ADDRESS:
            return ADDRESS_EDEFAULT == null ? address != null : !ADDRESS_EDEFAULT.equals(address);
        case ModelPackage.CONTACT_INFO__WEBSITE:
            return WEBSITE_EDEFAULT == null ? website != null : !WEBSITE_EDEFAULT.equals(website);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy())
            return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(", email: ");
        result.append(email);
        result.append(", phone: ");
        result.append(phone);
        result.append(", address: ");
        result.append(address);
        result.append(", website: ");
        result.append(website);
        result.append(')');
        return result.toString();
    }

} // ContactInfoImpl
