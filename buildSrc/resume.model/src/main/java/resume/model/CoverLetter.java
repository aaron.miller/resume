/**
 */
package resume.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Cover
 * Letter</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link resume.model.CoverLetter#getTitle <em>Title</em>}</li>
 * <li>{@link resume.model.CoverLetter#getContent <em>Content</em>}</li>
 * </ul>
 *
 * @see resume.model.ModelPackage#getCoverLetter()
 * @model
 * @generated
 */
public interface CoverLetter extends EObject {
    /**
     * Returns the value of the '<em><b>Title</b></em>' attribute. The default
     * value is <code>"Cover Letter"</code>. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @return the value of the '<em>Title</em>' attribute.
     * @see #setTitle(String)
     * @see resume.model.ModelPackage#getCoverLetter_Title()
     * @model default="Cover Letter"
     * @generated
     */
    String getTitle();

    /**
     * Sets the value of the '{@link resume.model.CoverLetter#getTitle
     * <em>Title</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Title</em>' attribute.
     * @see #getTitle()
     * @generated
     */
    void setTitle(String value);

    /**
     * Returns the value of the '<em><b>Content</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Content</em>' attribute.
     * @see #setContent(String)
     * @see resume.model.ModelPackage#getCoverLetter_Content()
     * @model
     * @generated
     */
    String getContent();

    /**
     * Sets the value of the '{@link resume.model.CoverLetter#getContent
     * <em>Content</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @param value
     *            the new value of the '<em>Content</em>' attribute.
     * @see #getContent()
     * @generated
     */
    void setContent(String value);

} // CoverLetter
