/**
 */
package resume.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object
 * '<em><b>Employer</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link resume.model.Employer#getName <em>Name</em>}</li>
 * <li>{@link resume.model.Employer#getContact <em>Contact</em>}</li>
 * </ul>
 *
 * @see resume.model.ModelPackage#getEmployer()
 * @model
 * @generated
 */
public interface Employer extends EObject {
    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see resume.model.ModelPackage#getEmployer_Name()
     * @model
     * @generated
     */
    String getName();

    /**
     * Sets the value of the '{@link resume.model.Employer#getName
     * <em>Name</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName(String value);

    /**
     * Returns the value of the '<em><b>Contact</b></em>' reference. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Contact</em>' reference.
     * @see #setContact(ContactInfo)
     * @see resume.model.ModelPackage#getEmployer_Contact()
     * @model
     * @generated
     */
    ContactInfo getContact();

    /**
     * Sets the value of the '{@link resume.model.Employer#getContact
     * <em>Contact</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @param value
     *            the new value of the '<em>Contact</em>' reference.
     * @see #getContact()
     * @generated
     */
    void setContact(ContactInfo value);

} // Employer
