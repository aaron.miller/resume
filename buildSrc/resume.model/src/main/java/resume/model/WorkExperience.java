/**
 */
package resume.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Work
 * Experience</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link resume.model.WorkExperience#getTitle <em>Title</em>}</li>
 * <li>{@link resume.model.WorkExperience#getPositions <em>Positions</em>}</li>
 * </ul>
 *
 * @see resume.model.ModelPackage#getWorkExperience()
 * @model
 * @generated
 */
public interface WorkExperience extends EObject {
    /**
     * Returns the value of the '<em><b>Title</b></em>' attribute. The default
     * value is <code>"Work Experience"</code>. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @return the value of the '<em>Title</em>' attribute.
     * @see #setTitle(String)
     * @see resume.model.ModelPackage#getWorkExperience_Title()
     * @model default="Work Experience"
     * @generated
     */
    String getTitle();

    /**
     * Sets the value of the '{@link resume.model.WorkExperience#getTitle
     * <em>Title</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Title</em>' attribute.
     * @see #getTitle()
     * @generated
     */
    void setTitle(String value);

    /**
     * Returns the value of the '<em><b>Positions</b></em>' containment
     * reference list. The list contents are of type
     * {@link resume.model.JobPosition}. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @return the value of the '<em>Positions</em>' containment reference list.
     * @see resume.model.ModelPackage#getWorkExperience_Positions()
     * @model containment="true"
     * @generated
     */
    EList<JobPosition> getPositions();

} // WorkExperience
