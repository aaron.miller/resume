/**
 */
package resume.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object
 * '<em><b>Resume</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link resume.model.Resume#getTitle <em>Title</em>}</li>
 * <li>{@link resume.model.Resume#getCoverLetter <em>Cover Letter</em>}</li>
 * <li>{@link resume.model.Resume#getWorkExperience <em>Work
 * Experience</em>}</li>
 * <li>{@link resume.model.Resume#getReference <em>Reference</em>}</li>
 * <li>{@link resume.model.Resume#getContact <em>Contact</em>}</li>
 * </ul>
 *
 * @see resume.model.ModelPackage#getResume()
 * @model
 * @generated
 */
public interface Resume extends EObject {
    /**
     * Returns the value of the '<em><b>Title</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Title</em>' attribute.
     * @see #setTitle(String)
     * @see resume.model.ModelPackage#getResume_Title()
     * @model
     * @generated
     */
    String getTitle();

    /**
     * Sets the value of the '{@link resume.model.Resume#getTitle
     * <em>Title</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Title</em>' attribute.
     * @see #getTitle()
     * @generated
     */
    void setTitle(String value);

    /**
     * Returns the value of the '<em><b>Cover Letter</b></em>' containment
     * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Cover Letter</em>' containment reference.
     * @see #setCoverLetter(CoverLetter)
     * @see resume.model.ModelPackage#getResume_CoverLetter()
     * @model containment="true" required="true"
     * @generated
     */
    CoverLetter getCoverLetter();

    /**
     * Sets the value of the '{@link resume.model.Resume#getCoverLetter
     * <em>Cover Letter</em>}' containment reference. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Cover Letter</em>' containment
     *            reference.
     * @see #getCoverLetter()
     * @generated
     */
    void setCoverLetter(CoverLetter value);

    /**
     * Returns the value of the '<em><b>Work Experience</b></em>' containment
     * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Work Experience</em>' containment
     *         reference.
     * @see #setWorkExperience(WorkExperience)
     * @see resume.model.ModelPackage#getResume_WorkExperience()
     * @model containment="true" required="true"
     * @generated
     */
    WorkExperience getWorkExperience();

    /**
     * Sets the value of the '{@link resume.model.Resume#getWorkExperience
     * <em>Work Experience</em>}' containment reference. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Work Experience</em>' containment
     *            reference.
     * @see #getWorkExperience()
     * @generated
     */
    void setWorkExperience(WorkExperience value);

    /**
     * Returns the value of the '<em><b>Reference</b></em>' containment
     * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Reference</em>' containment reference.
     * @see #setReference(Reference)
     * @see resume.model.ModelPackage#getResume_Reference()
     * @model containment="true" required="true"
     * @generated
     */
    Reference getReference();

    /**
     * Sets the value of the '{@link resume.model.Resume#getReference
     * <em>Reference</em>}' containment reference. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Reference</em>' containment
     *            reference.
     * @see #getReference()
     * @generated
     */
    void setReference(Reference value);

    /**
     * Returns the value of the '<em><b>Contact</b></em>' reference. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Contact</em>' reference.
     * @see #setContact(ContactInfo)
     * @see resume.model.ModelPackage#getResume_Contact()
     * @model required="true"
     * @generated
     */
    ContactInfo getContact();

    /**
     * Sets the value of the '{@link resume.model.Resume#getContact
     * <em>Contact</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @param value
     *            the new value of the '<em>Contact</em>' reference.
     * @see #getContact()
     * @generated
     */
    void setContact(ContactInfo value);

} // Resume
