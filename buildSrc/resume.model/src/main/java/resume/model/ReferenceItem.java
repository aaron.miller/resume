/**
 */
package resume.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object
 * '<em><b>Reference Item</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link resume.model.ReferenceItem#getDescription
 * <em>Description</em>}</li>
 * <li>{@link resume.model.ReferenceItem#getUri <em>Uri</em>}</li>
 * </ul>
 *
 * @see resume.model.ModelPackage#getReferenceItem()
 * @model
 * @generated
 */
public interface ReferenceItem extends EObject {
    /**
     * Returns the value of the '<em><b>Description</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Description</em>' attribute.
     * @see #setDescription(String)
     * @see resume.model.ModelPackage#getReferenceItem_Description()
     * @model
     * @generated
     */
    String getDescription();

    /**
     * Sets the value of the '{@link resume.model.ReferenceItem#getDescription
     * <em>Description</em>}' attribute. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Description</em>' attribute.
     * @see #getDescription()
     * @generated
     */
    void setDescription(String value);

    /**
     * Returns the value of the '<em><b>Uri</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Uri</em>' attribute.
     * @see #setUri(String)
     * @see resume.model.ModelPackage#getReferenceItem_Uri()
     * @model
     * @generated
     */
    String getUri();

    /**
     * Sets the value of the '{@link resume.model.ReferenceItem#getUri
     * <em>Uri</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Uri</em>' attribute.
     * @see #getUri()
     * @generated
     */
    void setUri(String value);

} // ReferenceItem
