/**
 */
package resume.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object
 * '<em><b>Accomplishment</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link resume.model.Accomplishment#getTitle <em>Title</em>}</li>
 * <li>{@link resume.model.Accomplishment#getDescription
 * <em>Description</em>}</li>
 * <li>{@link resume.model.Accomplishment#getSkills <em>Skills</em>}</li>
 * </ul>
 *
 * @see resume.model.ModelPackage#getAccomplishment()
 * @model
 * @generated
 */
public interface Accomplishment extends EObject {
    /**
     * Returns the value of the '<em><b>Title</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Title</em>' attribute.
     * @see #setTitle(String)
     * @see resume.model.ModelPackage#getAccomplishment_Title()
     * @model
     * @generated
     */
    String getTitle();

    /**
     * Sets the value of the '{@link resume.model.Accomplishment#getTitle
     * <em>Title</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Title</em>' attribute.
     * @see #getTitle()
     * @generated
     */
    void setTitle(String value);

    /**
     * Returns the value of the '<em><b>Skills</b></em>' reference list. The
     * list contents are of type {@link resume.model.Skill}. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Skills</em>' reference list.
     * @see resume.model.ModelPackage#getAccomplishment_Skills()
     * @model
     * @generated
     */
    EList<Skill> getSkills();

    /**
     * Returns the value of the '<em><b>Description</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Description</em>' attribute.
     * @see #setDescription(String)
     * @see resume.model.ModelPackage#getAccomplishment_Description()
     * @model
     * @generated
     */
    String getDescription();

    /**
     * Sets the value of the '{@link resume.model.Accomplishment#getDescription
     * <em>Description</em>}' attribute. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Description</em>' attribute.
     * @see #getDescription()
     * @generated
     */
    void setDescription(String value);

} // Accomplishment
