/**
 */
package resume.model;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see resume.model.ModelFactory
 * @model kind="package"
 * @generated
 */
public interface ModelPackage extends EPackage {
    /**
     * The package name. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "model";

    /**
     * The package namespace URI. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "http://www.waweb.io/resume";

    /**
     * The package namespace name. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "resume.model";

    /**
     * The singleton instance of the package. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    ModelPackage eINSTANCE = resume.model.impl.ModelPackageImpl.init();

    /**
     * The meta object id for the '{@link resume.model.impl.ResumeImpl
     * <em>Resume</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see resume.model.impl.ResumeImpl
     * @see resume.model.impl.ModelPackageImpl#getResume()
     * @generated
     */
    int RESUME = 0;

    /**
     * The feature id for the '<em><b>Title</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESUME__TITLE = 0;

    /**
     * The feature id for the '<em><b>Cover Letter</b></em>' containment
     * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESUME__COVER_LETTER = 1;

    /**
     * The feature id for the '<em><b>Work Experience</b></em>' containment
     * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESUME__WORK_EXPERIENCE = 2;

    /**
     * The feature id for the '<em><b>Reference</b></em>' containment reference.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESUME__REFERENCE = 3;

    /**
     * The feature id for the '<em><b>Contact</b></em>' reference. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESUME__CONTACT = 4;

    /**
     * The number of structural features of the '<em>Resume</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESUME_FEATURE_COUNT = 5;

    /**
     * The number of operations of the '<em>Resume</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int RESUME_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link resume.model.impl.CoverLetterImpl
     * <em>Cover Letter</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @see resume.model.impl.CoverLetterImpl
     * @see resume.model.impl.ModelPackageImpl#getCoverLetter()
     * @generated
     */
    int COVER_LETTER = 1;

    /**
     * The feature id for the '<em><b>Title</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COVER_LETTER__TITLE = 0;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COVER_LETTER__CONTENT = 1;

    /**
     * The number of structural features of the '<em>Cover Letter</em>' class.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COVER_LETTER_FEATURE_COUNT = 2;

    /**
     * The number of operations of the '<em>Cover Letter</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COVER_LETTER_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link resume.model.impl.WorkExperienceImpl
     * <em>Work Experience</em>}' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see resume.model.impl.WorkExperienceImpl
     * @see resume.model.impl.ModelPackageImpl#getWorkExperience()
     * @generated
     */
    int WORK_EXPERIENCE = 2;

    /**
     * The feature id for the '<em><b>Title</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int WORK_EXPERIENCE__TITLE = 0;

    /**
     * The feature id for the '<em><b>Positions</b></em>' containment reference
     * list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int WORK_EXPERIENCE__POSITIONS = 1;

    /**
     * The number of structural features of the '<em>Work Experience</em>'
     * class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int WORK_EXPERIENCE_FEATURE_COUNT = 2;

    /**
     * The number of operations of the '<em>Work Experience</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int WORK_EXPERIENCE_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link resume.model.impl.ReferenceImpl
     * <em>Reference</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see resume.model.impl.ReferenceImpl
     * @see resume.model.impl.ModelPackageImpl#getReference()
     * @generated
     */
    int REFERENCE = 3;

    /**
     * The feature id for the '<em><b>Title</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REFERENCE__TITLE = 0;

    /**
     * The feature id for the '<em><b>Items</b></em>' containment reference
     * list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REFERENCE__ITEMS = 1;

    /**
     * The number of structural features of the '<em>Reference</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REFERENCE_FEATURE_COUNT = 2;

    /**
     * The number of operations of the '<em>Reference</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REFERENCE_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link resume.model.impl.JobPositionImpl
     * <em>Job Position</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @see resume.model.impl.JobPositionImpl
     * @see resume.model.impl.ModelPackageImpl#getJobPosition()
     * @generated
     */
    int JOB_POSITION = 4;

    /**
     * The feature id for the '<em><b>Start Date</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int JOB_POSITION__START_DATE = 0;

    /**
     * The feature id for the '<em><b>End Date</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int JOB_POSITION__END_DATE = 1;

    /**
     * The feature id for the '<em><b>Title</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int JOB_POSITION__TITLE = 2;

    /**
     * The feature id for the '<em><b>Employer</b></em>' reference. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int JOB_POSITION__EMPLOYER = 3;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int JOB_POSITION__DESCRIPTION = 4;

    /**
     * The feature id for the '<em><b>Accomplishments</b></em>' containment
     * reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int JOB_POSITION__ACCOMPLISHMENTS = 5;

    /**
     * The number of structural features of the '<em>Job Position</em>' class.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int JOB_POSITION_FEATURE_COUNT = 6;

    /**
     * The number of operations of the '<em>Job Position</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int JOB_POSITION_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link resume.model.impl.ContactInfoImpl
     * <em>Contact Info</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @see resume.model.impl.ContactInfoImpl
     * @see resume.model.impl.ModelPackageImpl#getContactInfo()
     * @generated
     */
    int CONTACT_INFO = 5;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTACT_INFO__NAME = 0;

    /**
     * The feature id for the '<em><b>Email</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTACT_INFO__EMAIL = 1;

    /**
     * The feature id for the '<em><b>Phone</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTACT_INFO__PHONE = 2;

    /**
     * The feature id for the '<em><b>Address</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTACT_INFO__ADDRESS = 3;

    /**
     * The feature id for the '<em><b>Website</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTACT_INFO__WEBSITE = 4;

    /**
     * The number of structural features of the '<em>Contact Info</em>' class.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTACT_INFO_FEATURE_COUNT = 5;

    /**
     * The number of operations of the '<em>Contact Info</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTACT_INFO_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link resume.model.impl.EmployerImpl
     * <em>Employer</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see resume.model.impl.EmployerImpl
     * @see resume.model.impl.ModelPackageImpl#getEmployer()
     * @generated
     */
    int EMPLOYER = 6;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EMPLOYER__NAME = 0;

    /**
     * The feature id for the '<em><b>Contact</b></em>' reference. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EMPLOYER__CONTACT = 1;

    /**
     * The number of structural features of the '<em>Employer</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EMPLOYER_FEATURE_COUNT = 2;

    /**
     * The number of operations of the '<em>Employer</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EMPLOYER_OPERATION_COUNT = 0;


    /**
     * The meta object id for the '{@link resume.model.impl.AccomplishmentImpl
     * <em>Accomplishment</em>}' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see resume.model.impl.AccomplishmentImpl
     * @see resume.model.impl.ModelPackageImpl#getAccomplishment()
     * @generated
     */
    int ACCOMPLISHMENT = 7;

    /**
     * The feature id for the '<em><b>Title</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ACCOMPLISHMENT__TITLE = 0;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ACCOMPLISHMENT__DESCRIPTION = 1;

    /**
     * The feature id for the '<em><b>Skills</b></em>' reference list. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ACCOMPLISHMENT__SKILLS = 2;

    /**
     * The number of structural features of the '<em>Accomplishment</em>' class.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ACCOMPLISHMENT_FEATURE_COUNT = 3;

    /**
     * The number of operations of the '<em>Accomplishment</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ACCOMPLISHMENT_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link resume.model.impl.SkillImpl
     * <em>Skill</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see resume.model.impl.SkillImpl
     * @see resume.model.impl.ModelPackageImpl#getSkill()
     * @generated
     */
    int SKILL = 8;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SKILL__NAME = 0;

    /**
     * The number of structural features of the '<em>Skill</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SKILL_FEATURE_COUNT = 1;

    /**
     * The number of operations of the '<em>Skill</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SKILL_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link resume.model.impl.ReferenceItemImpl
     * <em>Reference Item</em>}' class. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see resume.model.impl.ReferenceItemImpl
     * @see resume.model.impl.ModelPackageImpl#getReferenceItem()
     * @generated
     */
    int REFERENCE_ITEM = 9;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REFERENCE_ITEM__DESCRIPTION = 0;

    /**
     * The feature id for the '<em><b>Uri</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REFERENCE_ITEM__URI = 1;

    /**
     * The number of structural features of the '<em>Reference Item</em>' class.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REFERENCE_ITEM_FEATURE_COUNT = 2;

    /**
     * The number of operations of the '<em>Reference Item</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REFERENCE_ITEM_OPERATION_COUNT = 0;


    /**
     * Returns the meta object for class '{@link resume.model.Resume
     * <em>Resume</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Resume</em>'.
     * @see resume.model.Resume
     * @generated
     */
    EClass getResume();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.Resume#getTitle <em>Title</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Title</em>'.
     * @see resume.model.Resume#getTitle()
     * @see #getResume()
     * @generated
     */
    EAttribute getResume_Title();

    /**
     * Returns the meta object for the containment reference
     * '{@link resume.model.Resume#getCoverLetter <em>Cover Letter</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Cover
     *         Letter</em>'.
     * @see resume.model.Resume#getCoverLetter()
     * @see #getResume()
     * @generated
     */
    EReference getResume_CoverLetter();

    /**
     * Returns the meta object for the containment reference
     * '{@link resume.model.Resume#getWorkExperience <em>Work Experience</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Work
     *         Experience</em>'.
     * @see resume.model.Resume#getWorkExperience()
     * @see #getResume()
     * @generated
     */
    EReference getResume_WorkExperience();

    /**
     * Returns the meta object for the containment reference
     * '{@link resume.model.Resume#getReference <em>Reference</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference
     *         '<em>Reference</em>'.
     * @see resume.model.Resume#getReference()
     * @see #getResume()
     * @generated
     */
    EReference getResume_Reference();

    /**
     * Returns the meta object for the containment reference
     * '{@link resume.model.Resume#getContact <em>Contact</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Contact</em>'.
     * @see resume.model.Resume#getContact()
     * @see #getResume()
     * @generated
     */
    EReference getResume_Contact();

    /**
     * Returns the meta object for class '{@link resume.model.CoverLetter
     * <em>Cover Letter</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Cover Letter</em>'.
     * @see resume.model.CoverLetter
     * @generated
     */
    EClass getCoverLetter();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.CoverLetter#getTitle <em>Title</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Title</em>'.
     * @see resume.model.CoverLetter#getTitle()
     * @see #getCoverLetter()
     * @generated
     */
    EAttribute getCoverLetter_Title();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.CoverLetter#getContent <em>Content</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Content</em>'.
     * @see resume.model.CoverLetter#getContent()
     * @see #getCoverLetter()
     * @generated
     */
    EAttribute getCoverLetter_Content();

    /**
     * Returns the meta object for class '{@link resume.model.WorkExperience
     * <em>Work Experience</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Work Experience</em>'.
     * @see resume.model.WorkExperience
     * @generated
     */
    EClass getWorkExperience();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.WorkExperience#getTitle <em>Title</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Title</em>'.
     * @see resume.model.WorkExperience#getTitle()
     * @see #getWorkExperience()
     * @generated
     */
    EAttribute getWorkExperience_Title();

    /**
     * Returns the meta object for the containment reference list
     * '{@link resume.model.WorkExperience#getPositions <em>Positions</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list
     *         '<em>Positions</em>'.
     * @see resume.model.WorkExperience#getPositions()
     * @see #getWorkExperience()
     * @generated
     */
    EReference getWorkExperience_Positions();

    /**
     * Returns the meta object for class '{@link resume.model.Reference
     * <em>Reference</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Reference</em>'.
     * @see resume.model.Reference
     * @generated
     */
    EClass getReference();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.Reference#getTitle <em>Title</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Title</em>'.
     * @see resume.model.Reference#getTitle()
     * @see #getReference()
     * @generated
     */
    EAttribute getReference_Title();

    /**
     * Returns the meta object for the containment reference list
     * '{@link resume.model.Reference#getItems <em>Items</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list
     *         '<em>Items</em>'.
     * @see resume.model.Reference#getItems()
     * @see #getReference()
     * @generated
     */
    EReference getReference_Items();

    /**
     * Returns the meta object for class '{@link resume.model.JobPosition
     * <em>Job Position</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Job Position</em>'.
     * @see resume.model.JobPosition
     * @generated
     */
    EClass getJobPosition();

    /**
     * Returns the meta object for the reference
     * '{@link resume.model.JobPosition#getEmployer <em>Employer</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Employer</em>'.
     * @see resume.model.JobPosition#getEmployer()
     * @see #getJobPosition()
     * @generated
     */
    EReference getJobPosition_Employer();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.JobPosition#getStartDate <em>Start Date</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Start Date</em>'.
     * @see resume.model.JobPosition#getStartDate()
     * @see #getJobPosition()
     * @generated
     */
    EAttribute getJobPosition_StartDate();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.JobPosition#getEndDate <em>End Date</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>End Date</em>'.
     * @see resume.model.JobPosition#getEndDate()
     * @see #getJobPosition()
     * @generated
     */
    EAttribute getJobPosition_EndDate();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.JobPosition#getTitle <em>Title</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Title</em>'.
     * @see resume.model.JobPosition#getTitle()
     * @see #getJobPosition()
     * @generated
     */
    EAttribute getJobPosition_Title();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.JobPosition#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see resume.model.JobPosition#getDescription()
     * @see #getJobPosition()
     * @generated
     */
    EAttribute getJobPosition_Description();

    /**
     * Returns the meta object for the containment reference list
     * '{@link resume.model.JobPosition#getAccomplishments
     * <em>Accomplishments</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list
     *         '<em>Accomplishments</em>'.
     * @see resume.model.JobPosition#getAccomplishments()
     * @see #getJobPosition()
     * @generated
     */
    EReference getJobPosition_Accomplishments();

    /**
     * Returns the meta object for class '{@link resume.model.ContactInfo
     * <em>Contact Info</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Contact Info</em>'.
     * @see resume.model.ContactInfo
     * @generated
     */
    EClass getContactInfo();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.ContactInfo#getName <em>Name</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see resume.model.ContactInfo#getName()
     * @see #getContactInfo()
     * @generated
     */
    EAttribute getContactInfo_Name();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.ContactInfo#getEmail <em>Email</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Email</em>'.
     * @see resume.model.ContactInfo#getEmail()
     * @see #getContactInfo()
     * @generated
     */
    EAttribute getContactInfo_Email();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.ContactInfo#getPhone <em>Phone</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Phone</em>'.
     * @see resume.model.ContactInfo#getPhone()
     * @see #getContactInfo()
     * @generated
     */
    EAttribute getContactInfo_Phone();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.ContactInfo#getAddress <em>Address</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Address</em>'.
     * @see resume.model.ContactInfo#getAddress()
     * @see #getContactInfo()
     * @generated
     */
    EAttribute getContactInfo_Address();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.ContactInfo#getWebsite <em>Website</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Website</em>'.
     * @see resume.model.ContactInfo#getWebsite()
     * @see #getContactInfo()
     * @generated
     */
    EAttribute getContactInfo_Website();

    /**
     * Returns the meta object for class '{@link resume.model.Employer
     * <em>Employer</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Employer</em>'.
     * @see resume.model.Employer
     * @generated
     */
    EClass getEmployer();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.Employer#getName <em>Name</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see resume.model.Employer#getName()
     * @see #getEmployer()
     * @generated
     */
    EAttribute getEmployer_Name();

    /**
     * Returns the meta object for the reference
     * '{@link resume.model.Employer#getContact <em>Contact</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Contact</em>'.
     * @see resume.model.Employer#getContact()
     * @see #getEmployer()
     * @generated
     */
    EReference getEmployer_Contact();

    /**
     * Returns the meta object for class '{@link resume.model.Accomplishment
     * <em>Accomplishment</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Accomplishment</em>'.
     * @see resume.model.Accomplishment
     * @generated
     */
    EClass getAccomplishment();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.Accomplishment#getTitle <em>Title</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Title</em>'.
     * @see resume.model.Accomplishment#getTitle()
     * @see #getAccomplishment()
     * @generated
     */
    EAttribute getAccomplishment_Title();

    /**
     * Returns the meta object for the reference list
     * '{@link resume.model.Accomplishment#getSkills <em>Skills</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Skills</em>'.
     * @see resume.model.Accomplishment#getSkills()
     * @see #getAccomplishment()
     * @generated
     */
    EReference getAccomplishment_Skills();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.Accomplishment#getDescription
     * <em>Description</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see resume.model.Accomplishment#getDescription()
     * @see #getAccomplishment()
     * @generated
     */
    EAttribute getAccomplishment_Description();

    /**
     * Returns the meta object for class '{@link resume.model.Skill
     * <em>Skill</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Skill</em>'.
     * @see resume.model.Skill
     * @generated
     */
    EClass getSkill();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.Skill#getName <em>Name</em>}'. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see resume.model.Skill#getName()
     * @see #getSkill()
     * @generated
     */
    EAttribute getSkill_Name();

    /**
     * Returns the meta object for class '{@link resume.model.ReferenceItem
     * <em>Reference Item</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Reference Item</em>'.
     * @see resume.model.ReferenceItem
     * @generated
     */
    EClass getReferenceItem();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.ReferenceItem#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see resume.model.ReferenceItem#getDescription()
     * @see #getReferenceItem()
     * @generated
     */
    EAttribute getReferenceItem_Description();

    /**
     * Returns the meta object for the attribute
     * '{@link resume.model.ReferenceItem#getUri <em>Uri</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Uri</em>'.
     * @see resume.model.ReferenceItem#getUri()
     * @see #getReferenceItem()
     * @generated
     */
    EAttribute getReferenceItem_Uri();

    /**
     * Returns the factory that creates the instances of the model. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    ModelFactory getModelFactory();

    /**
     * <!-- begin-user-doc --> Defines literals for the meta objects that
     * represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each operation of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals {
        /**
         * The meta object literal for the '{@link resume.model.impl.ResumeImpl
         * <em>Resume</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
         * -->
         * 
         * @see resume.model.impl.ResumeImpl
         * @see resume.model.impl.ModelPackageImpl#getResume()
         * @generated
         */
        EClass RESUME = eINSTANCE.getResume();

        /**
         * The meta object literal for the '<em><b>Title</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute RESUME__TITLE = eINSTANCE.getResume_Title();

        /**
         * The meta object literal for the '<em><b>Cover Letter</b></em>'
         * containment reference feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EReference RESUME__COVER_LETTER = eINSTANCE.getResume_CoverLetter();

        /**
         * The meta object literal for the '<em><b>Work Experience</b></em>'
         * containment reference feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EReference RESUME__WORK_EXPERIENCE = eINSTANCE.getResume_WorkExperience();

        /**
         * The meta object literal for the '<em><b>Reference</b></em>'
         * containment reference feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EReference RESUME__REFERENCE = eINSTANCE.getResume_Reference();

        /**
         * The meta object literal for the '<em><b>Contact</b></em>' reference
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference RESUME__CONTACT = eINSTANCE.getResume_Contact();

        /**
         * The meta object literal for the
         * '{@link resume.model.impl.CoverLetterImpl <em>Cover Letter</em>}'
         * class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see resume.model.impl.CoverLetterImpl
         * @see resume.model.impl.ModelPackageImpl#getCoverLetter()
         * @generated
         */
        EClass COVER_LETTER = eINSTANCE.getCoverLetter();

        /**
         * The meta object literal for the '<em><b>Title</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute COVER_LETTER__TITLE = eINSTANCE.getCoverLetter_Title();

        /**
         * The meta object literal for the '<em><b>Content</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute COVER_LETTER__CONTENT = eINSTANCE.getCoverLetter_Content();

        /**
         * The meta object literal for the
         * '{@link resume.model.impl.WorkExperienceImpl <em>Work
         * Experience</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
         * -->
         * 
         * @see resume.model.impl.WorkExperienceImpl
         * @see resume.model.impl.ModelPackageImpl#getWorkExperience()
         * @generated
         */
        EClass WORK_EXPERIENCE = eINSTANCE.getWorkExperience();

        /**
         * The meta object literal for the '<em><b>Title</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute WORK_EXPERIENCE__TITLE = eINSTANCE.getWorkExperience_Title();

        /**
         * The meta object literal for the '<em><b>Positions</b></em>'
         * containment reference list feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EReference WORK_EXPERIENCE__POSITIONS = eINSTANCE.getWorkExperience_Positions();

        /**
         * The meta object literal for the
         * '{@link resume.model.impl.ReferenceImpl <em>Reference</em>}' class.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see resume.model.impl.ReferenceImpl
         * @see resume.model.impl.ModelPackageImpl#getReference()
         * @generated
         */
        EClass REFERENCE = eINSTANCE.getReference();

        /**
         * The meta object literal for the '<em><b>Title</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute REFERENCE__TITLE = eINSTANCE.getReference_Title();

        /**
         * The meta object literal for the '<em><b>Items</b></em>' containment
         * reference list feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference REFERENCE__ITEMS = eINSTANCE.getReference_Items();

        /**
         * The meta object literal for the
         * '{@link resume.model.impl.JobPositionImpl <em>Job Position</em>}'
         * class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see resume.model.impl.JobPositionImpl
         * @see resume.model.impl.ModelPackageImpl#getJobPosition()
         * @generated
         */
        EClass JOB_POSITION = eINSTANCE.getJobPosition();

        /**
         * The meta object literal for the '<em><b>Employer</b></em>' reference
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference JOB_POSITION__EMPLOYER = eINSTANCE.getJobPosition_Employer();

        /**
         * The meta object literal for the '<em><b>Start Date</b></em>'
         * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute JOB_POSITION__START_DATE = eINSTANCE.getJobPosition_StartDate();

        /**
         * The meta object literal for the '<em><b>End Date</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute JOB_POSITION__END_DATE = eINSTANCE.getJobPosition_EndDate();

        /**
         * The meta object literal for the '<em><b>Title</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute JOB_POSITION__TITLE = eINSTANCE.getJobPosition_Title();

        /**
         * The meta object literal for the '<em><b>Description</b></em>'
         * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute JOB_POSITION__DESCRIPTION = eINSTANCE.getJobPosition_Description();

        /**
         * The meta object literal for the '<em><b>Accomplishments</b></em>'
         * containment reference list feature. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @generated
         */
        EReference JOB_POSITION__ACCOMPLISHMENTS = eINSTANCE.getJobPosition_Accomplishments();

        /**
         * The meta object literal for the
         * '{@link resume.model.impl.ContactInfoImpl <em>Contact Info</em>}'
         * class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see resume.model.impl.ContactInfoImpl
         * @see resume.model.impl.ModelPackageImpl#getContactInfo()
         * @generated
         */
        EClass CONTACT_INFO = eINSTANCE.getContactInfo();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONTACT_INFO__NAME = eINSTANCE.getContactInfo_Name();

        /**
         * The meta object literal for the '<em><b>Email</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONTACT_INFO__EMAIL = eINSTANCE.getContactInfo_Email();

        /**
         * The meta object literal for the '<em><b>Phone</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONTACT_INFO__PHONE = eINSTANCE.getContactInfo_Phone();

        /**
         * The meta object literal for the '<em><b>Address</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONTACT_INFO__ADDRESS = eINSTANCE.getContactInfo_Address();

        /**
         * The meta object literal for the '<em><b>Website</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONTACT_INFO__WEBSITE = eINSTANCE.getContactInfo_Website();

        /**
         * The meta object literal for the
         * '{@link resume.model.impl.EmployerImpl <em>Employer</em>}' class.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see resume.model.impl.EmployerImpl
         * @see resume.model.impl.ModelPackageImpl#getEmployer()
         * @generated
         */
        EClass EMPLOYER = eINSTANCE.getEmployer();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute EMPLOYER__NAME = eINSTANCE.getEmployer_Name();

        /**
         * The meta object literal for the '<em><b>Contact</b></em>' reference
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference EMPLOYER__CONTACT = eINSTANCE.getEmployer_Contact();

        /**
         * The meta object literal for the
         * '{@link resume.model.impl.AccomplishmentImpl
         * <em>Accomplishment</em>}' class. <!-- begin-user-doc --> <!--
         * end-user-doc -->
         * 
         * @see resume.model.impl.AccomplishmentImpl
         * @see resume.model.impl.ModelPackageImpl#getAccomplishment()
         * @generated
         */
        EClass ACCOMPLISHMENT = eINSTANCE.getAccomplishment();

        /**
         * The meta object literal for the '<em><b>Title</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ACCOMPLISHMENT__TITLE = eINSTANCE.getAccomplishment_Title();

        /**
         * The meta object literal for the '<em><b>Skills</b></em>' reference
         * list feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ACCOMPLISHMENT__SKILLS = eINSTANCE.getAccomplishment_Skills();

        /**
         * The meta object literal for the '<em><b>Description</b></em>'
         * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ACCOMPLISHMENT__DESCRIPTION = eINSTANCE.getAccomplishment_Description();

        /**
         * The meta object literal for the '{@link resume.model.impl.SkillImpl
         * <em>Skill</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see resume.model.impl.SkillImpl
         * @see resume.model.impl.ModelPackageImpl#getSkill()
         * @generated
         */
        EClass SKILL = eINSTANCE.getSkill();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SKILL__NAME = eINSTANCE.getSkill_Name();

        /**
         * The meta object literal for the
         * '{@link resume.model.impl.ReferenceItemImpl <em>Reference Item</em>}'
         * class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see resume.model.impl.ReferenceItemImpl
         * @see resume.model.impl.ModelPackageImpl#getReferenceItem()
         * @generated
         */
        EClass REFERENCE_ITEM = eINSTANCE.getReferenceItem();

        /**
         * The meta object literal for the '<em><b>Description</b></em>'
         * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute REFERENCE_ITEM__DESCRIPTION = eINSTANCE.getReferenceItem_Description();

        /**
         * The meta object literal for the '<em><b>Uri</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute REFERENCE_ITEM__URI = eINSTANCE.getReferenceItem_Uri();

    }

} // ModelPackage
