/**
 */
package resume.model;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * 
 * @see resume.model.ModelPackage
 * @generated
 */
public interface ModelFactory extends EFactory {
    /**
     * The singleton instance of the factory. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    ModelFactory eINSTANCE = resume.model.impl.ModelFactoryImpl.init();

    /**
     * Returns a new object of class '<em>Resume</em>'. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Resume</em>'.
     * @generated
     */
    Resume createResume();

    /**
     * Returns a new object of class '<em>Cover Letter</em>'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Cover Letter</em>'.
     * @generated
     */
    CoverLetter createCoverLetter();

    /**
     * Returns a new object of class '<em>Work Experience</em>'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Work Experience</em>'.
     * @generated
     */
    WorkExperience createWorkExperience();

    /**
     * Returns a new object of class '<em>Reference</em>'. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Reference</em>'.
     * @generated
     */
    Reference createReference();

    /**
     * Returns a new object of class '<em>Job Position</em>'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Job Position</em>'.
     * @generated
     */
    JobPosition createJobPosition();

    /**
     * Returns a new object of class '<em>Contact Info</em>'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Contact Info</em>'.
     * @generated
     */
    ContactInfo createContactInfo();

    /**
     * Returns a new object of class '<em>Employer</em>'. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Employer</em>'.
     * @generated
     */
    Employer createEmployer();

    /**
     * Returns a new object of class '<em>Accomplishment</em>'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Accomplishment</em>'.
     * @generated
     */
    Accomplishment createAccomplishment();

    /**
     * Returns a new object of class '<em>Skill</em>'. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Skill</em>'.
     * @generated
     */
    Skill createSkill();

    /**
     * Returns a new object of class '<em>Reference Item</em>'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Reference Item</em>'.
     * @generated
     */
    ReferenceItem createReferenceItem();

    /**
     * Returns the package supported by this factory. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the package supported by this factory.
     * @generated
     */
    ModelPackage getModelPackage();

} // ModelFactory
