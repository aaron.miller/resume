/**
 */
package resume.model;

import java.util.Date;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Job
 * Position</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link resume.model.JobPosition#getStartDate <em>Start Date</em>}</li>
 * <li>{@link resume.model.JobPosition#getEndDate <em>End Date</em>}</li>
 * <li>{@link resume.model.JobPosition#getTitle <em>Title</em>}</li>
 * <li>{@link resume.model.JobPosition#getEmployer <em>Employer</em>}</li>
 * <li>{@link resume.model.JobPosition#getDescription <em>Description</em>}</li>
 * <li>{@link resume.model.JobPosition#getAccomplishments
 * <em>Accomplishments</em>}</li>
 * </ul>
 *
 * @see resume.model.ModelPackage#getJobPosition()
 * @model
 * @generated
 */
public interface JobPosition extends EObject {
    /**
     * Returns the value of the '<em><b>Employer</b></em>' reference. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Employer</em>' reference.
     * @see #setEmployer(Employer)
     * @see resume.model.ModelPackage#getJobPosition_Employer()
     * @model required="true"
     * @generated
     */
    Employer getEmployer();

    /**
     * Sets the value of the '{@link resume.model.JobPosition#getEmployer
     * <em>Employer</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @param value
     *            the new value of the '<em>Employer</em>' reference.
     * @see #getEmployer()
     * @generated
     */
    void setEmployer(Employer value);

    /**
     * Returns the value of the '<em><b>Start Date</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Start Date</em>' attribute.
     * @see #setStartDate(Date)
     * @see resume.model.ModelPackage#getJobPosition_StartDate()
     * @model
     * @generated
     */
    Date getStartDate();

    /**
     * Sets the value of the '{@link resume.model.JobPosition#getStartDate
     * <em>Start Date</em>}' attribute. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Start Date</em>' attribute.
     * @see #getStartDate()
     * @generated
     */
    void setStartDate(Date value);

    /**
     * Returns the value of the '<em><b>End Date</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>End Date</em>' attribute.
     * @see #setEndDate(Date)
     * @see resume.model.ModelPackage#getJobPosition_EndDate()
     * @model
     * @generated
     */
    Date getEndDate();

    /**
     * Sets the value of the '{@link resume.model.JobPosition#getEndDate <em>End
     * Date</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>End Date</em>' attribute.
     * @see #getEndDate()
     * @generated
     */
    void setEndDate(Date value);

    /**
     * Returns the value of the '<em><b>Title</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Title</em>' attribute.
     * @see #setTitle(String)
     * @see resume.model.ModelPackage#getJobPosition_Title()
     * @model
     * @generated
     */
    String getTitle();

    /**
     * Sets the value of the '{@link resume.model.JobPosition#getTitle
     * <em>Title</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Title</em>' attribute.
     * @see #getTitle()
     * @generated
     */
    void setTitle(String value);

    /**
     * Returns the value of the '<em><b>Description</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Description</em>' attribute.
     * @see #setDescription(String)
     * @see resume.model.ModelPackage#getJobPosition_Description()
     * @model
     * @generated
     */
    String getDescription();

    /**
     * Sets the value of the '{@link resume.model.JobPosition#getDescription
     * <em>Description</em>}' attribute. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Description</em>' attribute.
     * @see #getDescription()
     * @generated
     */
    void setDescription(String value);

    /**
     * Returns the value of the '<em><b>Accomplishments</b></em>' containment
     * reference list. The list contents are of type
     * {@link resume.model.Accomplishment}. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @return the value of the '<em>Accomplishments</em>' containment reference
     *         list.
     * @see resume.model.ModelPackage#getJobPosition_Accomplishments()
     * @model containment="true"
     * @generated
     */
    EList<Accomplishment> getAccomplishments();

} // JobPosition
