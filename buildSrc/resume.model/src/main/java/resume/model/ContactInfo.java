/**
 */
package resume.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Contact
 * Info</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link resume.model.ContactInfo#getName <em>Name</em>}</li>
 * <li>{@link resume.model.ContactInfo#getEmail <em>Email</em>}</li>
 * <li>{@link resume.model.ContactInfo#getPhone <em>Phone</em>}</li>
 * <li>{@link resume.model.ContactInfo#getAddress <em>Address</em>}</li>
 * <li>{@link resume.model.ContactInfo#getWebsite <em>Website</em>}</li>
 * </ul>
 *
 * @see resume.model.ModelPackage#getContactInfo()
 * @model
 * @generated
 */
public interface ContactInfo extends EObject {
    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see resume.model.ModelPackage#getContactInfo_Name()
     * @model
     * @generated
     */
    String getName();

    /**
     * Sets the value of the '{@link resume.model.ContactInfo#getName
     * <em>Name</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName(String value);

    /**
     * Returns the value of the '<em><b>Email</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Email</em>' attribute.
     * @see #setEmail(String)
     * @see resume.model.ModelPackage#getContactInfo_Email()
     * @model
     * @generated
     */
    String getEmail();

    /**
     * Sets the value of the '{@link resume.model.ContactInfo#getEmail
     * <em>Email</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Email</em>' attribute.
     * @see #getEmail()
     * @generated
     */
    void setEmail(String value);

    /**
     * Returns the value of the '<em><b>Phone</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Phone</em>' attribute.
     * @see #setPhone(String)
     * @see resume.model.ModelPackage#getContactInfo_Phone()
     * @model
     * @generated
     */
    String getPhone();

    /**
     * Sets the value of the '{@link resume.model.ContactInfo#getPhone
     * <em>Phone</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value
     *            the new value of the '<em>Phone</em>' attribute.
     * @see #getPhone()
     * @generated
     */
    void setPhone(String value);

    /**
     * Returns the value of the '<em><b>Address</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Address</em>' attribute.
     * @see #setAddress(String)
     * @see resume.model.ModelPackage#getContactInfo_Address()
     * @model
     * @generated
     */
    String getAddress();

    /**
     * Sets the value of the '{@link resume.model.ContactInfo#getAddress
     * <em>Address</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @param value
     *            the new value of the '<em>Address</em>' attribute.
     * @see #getAddress()
     * @generated
     */
    void setAddress(String value);

    /**
     * Returns the value of the '<em><b>Website</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Website</em>' attribute.
     * @see #setWebsite(String)
     * @see resume.model.ModelPackage#getContactInfo_Website()
     * @model
     * @generated
     */
    String getWebsite();

    /**
     * Sets the value of the '{@link resume.model.ContactInfo#getWebsite
     * <em>Website</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @param value
     *            the new value of the '<em>Website</em>' attribute.
     * @see #getWebsite()
     * @generated
     */
    void setWebsite(String value);

} // ContactInfo
