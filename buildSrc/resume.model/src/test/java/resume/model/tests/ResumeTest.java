/**
 */
package resume.model.tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static resume.model.tests.ModelTests.printDiagnostic;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.junit.Test;

import junit.framework.TestCase;
import junit.textui.TestRunner;
import resume.model.ModelFactory;
import resume.model.Resume;

/**
 * <!-- begin-user-doc --> A test case for the model object
 * '<em><b>Resume</b></em>'. <!-- end-user-doc -->
 * 
 * @generated
 */
public class ResumeTest extends TestCase {

    @Test
    public void test() {
        final ModelFactory factory = ModelFactory.eINSTANCE;
        fixture.setContact(factory.createContactInfo());
        fixture.setCoverLetter(factory.createCoverLetter());
        fixture.setWorkExperience(factory.createWorkExperience());
        fixture.setReference(factory.createReference());

        Diagnostic result = Diagnostician.INSTANCE.validate(fixture);
        printDiagnostic(result);
        assertThat("MAY be constructed", result.getSeverity(), equalTo(Diagnostic.OK));
    }

    /**
     * The fixture for this Resume test case. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    protected Resume fixture = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main(String[] args) {
        TestRunner.run(ResumeTest.class);
    }

    /**
     * Constructs a new Resume test case with the given name. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public ResumeTest(String name) {
        super(name);
    }

    /**
     * Sets the fixture for this Resume test case. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture(Resume fixture) {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Resume test case. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Resume getFixture() {
        return fixture;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp() throws Exception {
        setFixture(ModelFactory.eINSTANCE.createResume());
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown() throws Exception {
        setFixture(null);
    }

} // ResumeTest
