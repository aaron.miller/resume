/**
 */
package resume.model.tests;

import org.eclipse.emf.common.util.Diagnostic;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc --> A test suite for the '<em><b>model</b></em>' package.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ModelTests extends TestSuite {

    public static void printDiagnostic(final Diagnostic diagnostic) {
        if (diagnostic == null || diagnostic.getSeverity() == Diagnostic.OK) {
            return;
        }

        System.out.println(diagnostic.getMessage());
        for (final Diagnostic d : diagnostic.getChildren()) {
            printDiagnostic(d);
        }
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main(String[] args) {
        TestRunner.run(suite());
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static Test suite() {
        TestSuite suite = new ModelTests("model Tests");
        return suite;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public ModelTests(String name) {
        super(name);
    }

} // ModelTests
