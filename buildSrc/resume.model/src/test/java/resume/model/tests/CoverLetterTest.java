/**
 */
package resume.model.tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.junit.Test;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import resume.model.CoverLetter;
import resume.model.ModelFactory;

/**
 * <!-- begin-user-doc --> A test case for the model object '<em><b>Cover
 * Letter</b></em>'. <!-- end-user-doc -->
 * 
 * @generated
 */
public class CoverLetterTest extends TestCase {

    @Test
    public void test() {
        Diagnostic result = Diagnostician.INSTANCE.validate(fixture);
        assertThat("MAY be constructed", result.getSeverity(), equalTo(Diagnostic.OK));
    }

    /**
     * The fixture for this Cover Letter test case. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    protected CoverLetter fixture = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main(String[] args) {
        TestRunner.run(CoverLetterTest.class);
    }

    /**
     * Constructs a new Cover Letter test case with the given name. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public CoverLetterTest(String name) {
        super(name);
    }

    /**
     * Sets the fixture for this Cover Letter test case. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture(CoverLetter fixture) {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Cover Letter test case. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected CoverLetter getFixture() {
        return fixture;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp() throws Exception {
        setFixture(ModelFactory.eINSTANCE.createCoverLetter());
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown() throws Exception {
        setFixture(null);
    }

} // CoverLetterTest
