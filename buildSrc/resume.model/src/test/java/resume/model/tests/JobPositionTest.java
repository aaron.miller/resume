/**
 */
package resume.model.tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static resume.model.tests.ModelTests.printDiagnostic;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.junit.Test;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import resume.model.JobPosition;
import resume.model.ModelFactory;

/**
 * <!-- begin-user-doc --> A test case for the model object '<em><b>Job
 * Position</b></em>'. <!-- end-user-doc -->
 * 
 * @generated
 */
public class JobPositionTest extends TestCase {

    @Test
    public void test() {
        final ModelFactory factory = ModelFactory.eINSTANCE;
        fixture.setEmployer(factory.createEmployer());

        Diagnostic result = Diagnostician.INSTANCE.validate(fixture);
        printDiagnostic(result);
        assertThat("MAY be constructed", result.getSeverity(), equalTo(Diagnostic.OK));
    }

    /**
     * The fixture for this Job Position test case. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    protected JobPosition fixture = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main(String[] args) {
        TestRunner.run(JobPositionTest.class);
    }

    /**
     * Constructs a new Job Position test case with the given name. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public JobPositionTest(String name) {
        super(name);
    }

    /**
     * Sets the fixture for this Job Position test case. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture(JobPosition fixture) {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Job Position test case. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected JobPosition getFixture() {
        return fixture;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp() throws Exception {
        setFixture(ModelFactory.eINSTANCE.createJobPosition());
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown() throws Exception {
        setFixture(null);
    }

} // JobPositionTest
