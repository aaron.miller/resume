/**
 */
package resume.model.tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.junit.Test;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import resume.model.ModelFactory;
import resume.model.Skill;

/**
 * <!-- begin-user-doc --> A test case for the model object
 * '<em><b>Skill</b></em>'. <!-- end-user-doc -->
 * 
 * @generated
 */
public class SkillTest extends TestCase {

    @Test
    public void test() {
        Diagnostic result = Diagnostician.INSTANCE.validate(fixture);
        assertThat("MAY be constructed", result.getSeverity(), equalTo(Diagnostic.OK));
    }

    /**
     * The fixture for this Skill test case. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    protected Skill fixture = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main(String[] args) {
        TestRunner.run(SkillTest.class);
    }

    /**
     * Constructs a new Skill test case with the given name. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public SkillTest(String name) {
        super(name);
    }

    /**
     * Sets the fixture for this Skill test case. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture(Skill fixture) {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Skill test case. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Skill getFixture() {
        return fixture;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp() throws Exception {
        setFixture(ModelFactory.eINSTANCE.createSkill());
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown() throws Exception {
        setFixture(null);
    }

} // SkillTest
