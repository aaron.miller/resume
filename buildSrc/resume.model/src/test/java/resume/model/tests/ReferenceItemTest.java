/**
 */
package resume.model.tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.junit.Test;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import resume.model.ModelFactory;
import resume.model.ReferenceItem;

/**
 * <!-- begin-user-doc --> A test case for the model object '<em><b>Reference
 * Item</b></em>'. <!-- end-user-doc -->
 * 
 * @generated
 */
public class ReferenceItemTest extends TestCase {

    @Test
    public void test() {
        Diagnostic result = Diagnostician.INSTANCE.validate(fixture);
        assertThat("MAY be constructed", result.getSeverity(), equalTo(Diagnostic.OK));
    }

    /**
     * The fixture for this Reference Item test case. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ReferenceItem fixture = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static void main(String[] args) {
        TestRunner.run(ReferenceItemTest.class);
    }

    /**
     * Constructs a new Reference Item test case with the given name. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public ReferenceItemTest(String name) {
        super(name);
    }

    /**
     * Sets the fixture for this Reference Item test case. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void setFixture(ReferenceItem fixture) {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Reference Item test case. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ReferenceItem getFixture() {
        return fixture;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp() throws Exception {
        setFixture(ModelFactory.eINSTANCE.createReferenceItem());
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown() throws Exception {
        setFixture(null);
    }

} // ReferenceItemTest
