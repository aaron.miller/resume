/**
 * 
 */
package resume.gradle

import javax.inject.Inject

import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.gradle.api.DefaultTask
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskAction

import resume.gradle.dsl.ResumeDsl

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 * 
 * Eclipse ECore models do not play well with task inputs, so we must convert it to a Map instance here
 *
 */
class ResumeModelMapper extends DefaultTask {

    /**
     * Takes a "live" model instance of EObject and returns a Map representation of the data.
     * These maps are not frozen (immutable), but should be treated as so. Perhaps I will
     * add this later.
     *
     * @param model
     * @return
     */
    static Map<String, Object> toMap(EObject model) {
        if(!model) return null

        final Map<String, Object> m = [:]

        model.eClass().getEAllAttributes().each { EAttribute attr ->
            m.putAt(attr.getName(), model.eGet(attr))
        }
        model.eClass().getEAllReferences().each { EReference ref ->
            if(ref.isMany()) {
                m.putAt(ref.getName(), model.eGet(ref).collect(ResumeModelMapper.&toMap))
            }
            else {
                m.putAt(ref.getName(), toMap(model.eGet(ref)))
            }
        }

        return m
    }

    @Internal
    final Property<ResumeDsl> resume = objectFactory.property(ResumeDsl)

    protected Map _model

    @Internal
    final Provider<Map> modelProvider = providerFactory.provider {
        return _model
    }

    @Inject
    ObjectFactory getObjectFactory() {}

    @Inject
    ProviderFactory getProviderFactory() {}


    @TaskAction
    void run() {
        _model = toMap(this.resume.get().getModel())
        logger.info("Loaded: ${_model.get('title')}")
    }
}
