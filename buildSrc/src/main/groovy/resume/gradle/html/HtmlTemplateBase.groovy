/**
 * 
 */
package resume.gradle.html

import com.vladsch.flexmark.ext.gfm.strikethrough.StrikethroughExtension
import com.vladsch.flexmark.ext.tables.TablesExtension
import com.vladsch.flexmark.html.HtmlRenderer
import com.vladsch.flexmark.parser.Parser
import com.vladsch.flexmark.util.ast.Node
import com.vladsch.flexmark.util.data.MutableDataSet

import groovy.text.markup.BaseTemplate
import groovy.text.markup.MarkupTemplateEngine
import groovy.text.markup.TemplateConfiguration

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
abstract class HtmlTemplateBase extends BaseTemplate {

    final Parser markdownParser

    final HtmlRenderer markdownRenderer

    /**
     * @param templateEngine
     * @param model
     * @param modelTypes
     * @param configuration
     */
    public HtmlTemplateBase(MarkupTemplateEngine templateEngine, Map model, Map<String, String> modelTypes,
    TemplateConfiguration configuration) {
        super(templateEngine, model, modelTypes, configuration)

        final MutableDataSet options = new MutableDataSet()

        // uncomment to set optional extensions
        options.set(Parser.EXTENSIONS,
                Arrays.asList(TablesExtension.create(), StrikethroughExtension.create()))

        // uncomment to convert soft-breaks to hard breaks
        //options.set(HtmlRenderer.SOFT_BREAK, "<br />\n");

        markdownParser = Parser.builder(options).build()
        markdownRenderer = HtmlRenderer.builder(options).build()
    }


    /**
     * Renders the markdown String content as html, and writes it directly to the output stream
     * @param obj
     * @return
     * @throws IOException
     */
    public HtmlTemplateBase yieldMarkdown(Object obj) throws IOException {

        if(!obj) return this

        // You can re-use parser and renderer instances
        final Node document = markdownParser.parse(obj as String)
        markdownRenderer.render(document, this.getOut())

        return this
    }
}
