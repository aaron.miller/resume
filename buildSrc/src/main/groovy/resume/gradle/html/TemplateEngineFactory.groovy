/**
 * 
 */
package resume.gradle.html

import groovy.text.markup.MarkupTemplateEngine
import groovy.text.markup.TemplateConfiguration

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 * Builder class used to construct an appropriate Groovy TemplateEngine.
 * See: http://docs.groovy-lang.org/docs/next/html/documentation/template-engines.html
 */
class TemplateEngineFactory {

    /**
     * @param classLoader
     * @param templateDir
     * @return
     */
    static MarkupTemplateEngine createHtmlTemplateEngine(final ClassLoader classLoader, final File templateDir) {
        final TemplateConfiguration config = new TemplateConfiguration()
        config.autoIndent = true
        config.autoNewLine = true
        config.expandEmptyElements = true
        config.setBaseTemplateClass(HtmlTemplateBase)

        return new MarkupTemplateEngine(classLoader, templateDir, config)
    }
}
