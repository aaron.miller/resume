/**
 * 
 */
package resume.gradle.html

import javax.inject.Inject

import org.gradle.api.DefaultTask
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.file.ProjectLayout
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

import groovy.text.Template
import groovy.text.TemplateEngine
import resume.gradle.dsl.ResumeDsl
import resume.model.Resume

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class RenderResumeHtml extends DefaultTask {

    /**
     * TODO this is a dummy input so that incremental task support
     * will correctly detect changes defined by the ResumeExtension.
     * This should eventually be replaced with an appropriate
     * implementation for ECore model serialization.
     */
    @Input
    Provider<Map> modelProvider

    @Internal
    final Property<ResumeDsl> resume = objectFactory.property(ResumeDsl)

    @InputDirectory
    final DirectoryProperty templateDir = objectFactory.directoryProperty()

    @Internal
    final DirectoryProperty outputDir = objectFactory.directoryProperty()

    @OutputFile
    final Provider<File> indexOutput = providerFactory.provider {
        return outputDir.file('index.html').get().asFile
    }

    @OutputFile
    final Provider<File> coverLetterOutput = providerFactory.provider {
        return outputDir.file('cover_letter.html').get().asFile
    }

    @OutputFile
    final Provider<File> workExperienceOutput = providerFactory.provider {
        return outputDir.file('work_experience.html').get().asFile
    }

    @OutputFile
    final Provider<File> referenceOutput = providerFactory.provider {
        return outputDir.file('references.html').get().asFile
    }

    @Inject
    ProviderFactory getProviderFactory() {}

    @Inject
    ObjectFactory getObjectFactory() {}

    /**
     * 
     */
    @Inject
    public RenderResumeHtml(ProjectLayout layout) {
        super()

        outputDir.convention(layout.buildDirectory.dir('resume'))
    }

    /**
     * 
     */
    @TaskAction
    void run() {

        //Construct TemplateEngine
        final File templateDir = this.templateDir.get().asFile
        final ClassLoader classLoader = Thread.currentThread().getContextClassLoader()
        final TemplateEngine engine = TemplateEngineFactory
                .createHtmlTemplateEngine(classLoader, templateDir)

        //TODO Use the typesafe ECore model in the Groovy template engine for faster serialization
        final Map model = [resume: this.modelProvider.get()]

        //Write out each page as HTML
        //TODO parralize with Worker API
        writePage(engine, model, 'index.groovy', this.indexOutput.get())
        writePage(engine, model, 'cover_letter.groovy', this.coverLetterOutput.get())
        writePage(engine, model, 'work_experience.groovy', this.workExperienceOutput.get())
        writePage(engine, model, 'references.groovy', this.referenceOutput.get())

        logger.info("Rendering HTML to ${this.outputDir.get().asFile}")
    }

    /**
     * Renders the resume index.html file
     * 
     * @param engine
     * @param model
     */
    void writePage(final TemplateEngine engine, final Map model, String templateName, File outputFile) {
        final File templateFile = this.templateDir.file(templateName).get().asFile
        templateFile.withReader { Reader reader ->
            final Template template = engine.createTemplate(reader)
            outputFile.withWriter { Writer writer ->
                template.make(model).writeTo(writer)
            }
        }
    }
}
