/**
 * 
 */
package resume.gradle

import javax.inject.Inject

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.gradle.api.DefaultTask
import org.gradle.api.file.ProjectLayout
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

import resume.gradle.dsl.ResumeDsl
import resume.model.ModelPackage

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class RenderResumeXmi extends DefaultTask {

    static final String FILE_ENCODING = 'UTF-8'

    /**
     * TODO this is a dummy input so that incremental task support
     * will correctly detect changes defined by the ResumeExtension.
     * This should eventually be replaced with an appropriate
     * implementation for ECore model serialization.
     */
    @Input
    Provider<Map> modelProvider

    @Internal
    final Property<ResumeDsl> resume = objectFactory.property(ResumeDsl)

    @OutputFile
    final RegularFileProperty outputFile = objectFactory.fileProperty()

    @Inject
    ObjectFactory getObjectFactory() {}

    /**
     * 
     */
    @Inject
    public RenderResumeXmi(ProjectLayout layout) {
        super()

        outputFile.convention(layout.projectDirectory.file('src/main/resources/resume.xmi'))
    }

    @TaskAction
    void run() {

        final ResumeDsl resume = this.resume.get()
        final File outputFile = this.outputFile.get().asFile

        // Create a resource set to hold the resources.
        final ResourceSet resourceSet = new ResourceSetImpl()

        // Register the appropriate resource factory to handle all file
        // extensions.
        resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
                .put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl())

        // Register the package to ensure it is available during loading.
        resourceSet.getPackageRegistry()
                .put(ModelPackage.eNS_URI, ModelPackage.eINSTANCE)

        // Create a new resource from the output file and append the model instances
        // to the resource contents
        final URI uri = URI.createURI('file://resume.xmi')
        final Resource resource = resourceSet.createResource(uri)



        resource.getContents().add(resume.getModel())
        resume.getModelRefs().each { EObject model ->
            resource.contents.add(model)
        }

        this.outputFile.get().asFile.withWriter { Writer writer ->
            resource.save(writer, null)
        }

        logger.info("Rendered Resume XMI: ${outputFile}")
    }
}
