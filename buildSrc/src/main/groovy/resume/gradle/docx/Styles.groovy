/**
 * 
 */
package resume.gradle.docx

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class Styles {

    static final public String NORMAL = 'Normal'

    static final public String TITLE = 'Title'

    static final public String SUB_TITLE = 'Subtitle'

    static final public String HEADING_1 = 'Heading1'

    static final public String HEADING_2 = 'Heading2'

    static final public String HEADING_3 = 'Heading3'

    static final public String HEADING_4 = 'Heading4'

    static final public String HEADING_5 = 'Heading5'
    
    static final public String PREFORMATTED_TEXT = 'PreformattedText'
}
