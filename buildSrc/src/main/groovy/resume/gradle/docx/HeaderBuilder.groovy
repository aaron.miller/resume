/**
 * 
 */
package resume.gradle.docx

import org.apache.poi.util.Units
import org.apache.poi.xwpf.usermodel.XWPFDocument

import resume.model.Resume

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class HeaderBuilder extends BaseNodeBuilder {

    static final int IMG_WIDTH = 240

    static final int IMG_HEIGHT = 180

    protected final Resume _model

    protected final File _skillCloudResource

    HeaderBuilder(XWPFDocument document, Resume model, File skillCloudResource) {
        super(document)

        _model = model
        _skillCloudResource = skillCloudResource
    }

    @Override
    void build() {
        addTable {
            //initialize table
            removeBorders()
            getRow(0).addNewTableCell()
            getRow(0).getCell(0).removeParagraph(0)
            getRow(0).getCell(1).removeParagraph(0)

            //Add main title
            getRow(0).getCell(0).addParagraph().tap {
                style = Styles.TITLE
                createRun().with { text = "Aaron R Miller" }
            }

            //add contact info
            // TODO - this should come from Resume model
            getRow(0).getCell(0).addParagraph().tap {
                style = Styles.SUB_TITLE
                createRun().with {
                    setText("Chief Technology Officer")
                    addBreak()
                    setText("Washington Web Apps, LLC")
                    addBreak()
                    setText("aaron.miller@waweb.io")
                    addBreak()
                    setText("(509) 996 - 7342")
                    addBreak()
                    setText("http://resume.waweb.io/")
                }
            }

            // Add Objective heading
            getRow(0).getCell(0).addParagraph().tap {
                style = Styles.HEADING_4
                createRun().with {
                    text = "Objective"
                }
            }

            // Add Objective Text
            getRow(0).getCell(0).addParagraph().tap {
                style = Styles.NORMAL
                createRun().with {
                    text = "Seeking new opportunities to develop software on a 1099 or C2C basis"
                }
            }

            // Add SkillCloud image
            getRow(0).getCell(1).addParagraph().tap {
                createRun().with {
                    addPicture(_skillCloudResource.newInputStream(),
                            XWPFDocument.PICTURE_TYPE_PNG,
                            "skill-cloud.png",
                            Units.pixelToEMU(IMG_WIDTH), 
                            Units.pixelToEMU(IMG_HEIGHT))
                }
            }
        }
    }
}
