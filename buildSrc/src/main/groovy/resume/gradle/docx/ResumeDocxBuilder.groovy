/**
 * 
 */
package resume.gradle.docx

import org.apache.poi.xwpf.usermodel.ParagraphAlignment
import org.apache.poi.xwpf.usermodel.XWPFDocument
import org.apache.poi.xwpf.usermodel.XWPFParagraph
import org.apache.poi.xwpf.usermodel.XWPFTable
import org.apache.poi.xwpf.usermodel.XWPFTableRow
import resume.model.Resume

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class ResumeDocxBuilder extends BaseNodeBuilder {

    protected final Resume _model

    protected final File _skillCloudResource
    
    ResumeDocxBuilder(XWPFDocument document, Resume model, File skillCloudResource) {
        super(document)
        
        _model = model
        _skillCloudResource = skillCloudResource
    }

    @Override
    void build() {
        //remove empty first paragraph
        _document.removeBodyElement(0)

        // Attach the top header table
        new HeaderBuilder(_document, _model, _skillCloudResource).build()

        // Attach the work experience sections
        new WorkExperienceBuilder(_document, _model.workExperience).build()

        // Commit the changes and lock the document model
        _document.commit()
    }

    XWPFDocument createDocx() {
        build()

        return _document
    }
}
