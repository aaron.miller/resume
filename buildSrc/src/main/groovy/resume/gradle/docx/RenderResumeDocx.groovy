/**
 * 
 */
package resume.gradle.docx

import javax.inject.Inject

import org.apache.poi.xwpf.usermodel.XWPFDocument
import org.gradle.api.DefaultTask
import org.gradle.api.file.ProjectLayout
import org.gradle.api.file.RegularFile
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskExecutionException

import resume.model.Resume

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class RenderResumeDocx extends DefaultTask {

    /**
     * TODO this is a dummy input so that incremental task support
     * will correctly detect changes defined by the ResumeExtension.
     * This should eventually be replaced with an appropriate
     * implementation for ECore model serialization.
     */
    @Input
    Provider<Map> modelProvider

    @Internal
    final Property<Resume> resume = objectFactory.property(Resume)
    
    @InputFile
    final RegularFileProperty skillCloudFile = objectFactory.fileProperty()

    @InputFile
    final RegularFileProperty templateFile = objectFactory.fileProperty()

    @OutputFile
    final RegularFileProperty outputFile = objectFactory.fileProperty()

    @Inject
    ObjectFactory getObjectFactory() {}

    /**
     * 
     */
    @Inject
    public RenderResumeDocx(ProjectLayout layout) {
        super()

        final RegularFile outputFile = layout.buildDirectory.get()
                .file("resume/${project.group}-${project.name}.docx")
        this.outputFile.convention(outputFile)

        final RegularFile templateFile = layout.projectDirectory
                .file("src/main/resources/template/docx/${project.name}.docx")
        this.templateFile.convention(templateFile)
    }

    @TaskAction
    void run() {

        XWPFDocument templateDoc = null

        // Read in the template document from a stream then close
        final File templateFile = this.templateFile.get().asFile
        templateFile.withInputStream { templateDoc = new XWPFDocument(it) }

        // Raise an exception if the resource failed to read
        // Note: Gradle SHOULD have asserted the file exists before we got to this point
        //       via the @InputFile annotation on the task property
        if(!templateDoc) {
            throw new TaskExecutionException(this, new RuntimeException("Unable to parse template input file at ${templateFile}"))
        }

        // Build a document node from the Resume model
        final Resume resume = this.resume.get()
        final File skillCloudResource = this.skillCloudFile.get().asFile
        final XWPFDocument document = new ResumeDocxBuilder(templateDoc, resume, skillCloudResource).createDocx()

        // Render the document to the output file and close the stream
        final File outputFile = this.outputFile.get().asFile
        outputFile.withOutputStream { document.write(it) }

        logger.info("Rendered Resume Docx: ${outputFile}")
    }
}
