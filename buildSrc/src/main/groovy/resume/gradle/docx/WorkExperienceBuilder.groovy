/**
 * 
 */
package resume.gradle.docx

import java.text.SimpleDateFormat

import org.apache.poi.xwpf.usermodel.XWPFDocument

import resume.model.Accomplishment
import resume.model.JobPosition
import resume.model.WorkExperience
import resume.model.Skill

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class WorkExperienceBuilder extends BaseNodeBuilder {

    private static final SimpleDateFormat _dateFormat = new SimpleDateFormat("MMM YYYY")
    
    /**
     * Returns a formatted string containing the number of years and months
     * between the two dates
     * TODO code duplication in work_experience template. Refactor into util method.
     * 
     * @param startDate
     * @param endDate
     * @return
     */
    static String toDurationString(Date startDate, Date endDate) {
        if(!startDate) {
            return "0 mo"
        }
        
        if(!endDate) {
            return "${_dateFormat.format(startDate)} - CURRENT"
        }

        return "${_dateFormat.format(startDate)} - ${_dateFormat.format(endDate)}"
    }

    /**
     * Returns the formatted header text for a given JobPosition
     * 
     * @param position
     * @return
     */
    static String toPositionTitle(final JobPosition position) {
        return "${position.title} @ ${position.employer?.name ?: ''} (${toDurationString(position.startDate, position.endDate)})"
    }

    protected final WorkExperience _model

    /**
     * @param document
     * @param model
     */
    WorkExperienceBuilder(XWPFDocument document, WorkExperience model) {
        super(document)

        _model = model
    }

    /**
     *
     */
    @Override
    void build() {
        // Create Header
        addParagraph {
            style = Styles.HEADING_1
            createRun().with { text = _model.title }
        }

        // Loop through each position while appending to the document
        _model.positions.listIterator()
                // Sort in reverse order by start date
                .sort({ a, b -> (b.startDate) <=> (a.startDate) })
                .each(this.&visitJobPosition)

    }

    void visitJobPosition(final JobPosition position) {

        // Add Position header if title exists
        addParagraph {
            style = Styles.HEADING_2
            createRun().with { text = toPositionTitle(position) }
        }

        // Add Position Description
        addMarkdown(position.description)

        // Add Accomplishment sections
        position.accomplishments.listIterator()
                .each(this.&visitAccomplishment)
    }

    void visitAccomplishment(final Accomplishment accomp) {

        // Add Accomplishment heading if title was assigned
        if(accomp.title) {
            addParagraph {
                style = Styles.HEADING_3
                createRun().with { text = accomp.title }
            }
        }

        // Add Accomplishment description
        addMarkdown(accomp.description)

        // Add a paragraph for the skills, separated by comma
        addParagraph {
            style = Styles.PREFORMATTED_TEXT
            createRun().with {
                text = accomp.skills.listIterator()
                        .collect({ Skill skill -> skill.name })
                        .join(', ')

            }
        }
    }
}
