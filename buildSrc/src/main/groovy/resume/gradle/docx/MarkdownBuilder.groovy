/**
 * 
 */
package resume.gradle.docx

import org.apache.poi.xwpf.usermodel.XWPFDocument
import org.apache.poi.xwpf.usermodel.XWPFParagraph
import org.apache.poi.xwpf.usermodel.XWPFRun

import com.vladsch.flexmark.ast.Paragraph
import com.vladsch.flexmark.ast.Text
import com.vladsch.flexmark.ext.gfm.strikethrough.StrikethroughExtension
import com.vladsch.flexmark.ext.tables.TablesExtension
import com.vladsch.flexmark.parser.Parser
import com.vladsch.flexmark.util.ast.Node
import com.vladsch.flexmark.util.ast.NodeVisitor
import com.vladsch.flexmark.util.ast.VisitHandler
import com.vladsch.flexmark.util.data.MutableDataSet

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class MarkdownBuilder extends BaseNodeBuilder {

    private static Parser _markdownParser

    private static Parser getMarkdownParser() {
        if(!_markdownParser) {
            final MutableDataSet options = new MutableDataSet()

            // uncomment to set optional extensions
            options.set(Parser.EXTENSIONS, [
                TablesExtension.create(),
                StrikethroughExtension.create()
            ])

            // uncomment to convert soft-breaks to hard breaks
            //options.set(HtmlRenderer.SOFT_BREAK, "<br />\n");

            _markdownParser = Parser.builder(options).build()
        }

        return _markdownParser
    }

    protected final NodeVisitor _nodeVisitor = new NodeVisitor(
            new VisitHandler<>(Paragraph.class, this.&visitMarkdownNodes)
    )

    protected final String _content

    MarkdownBuilder(final XWPFDocument document, final String content) {
        super(document)

        _content = content
    }

    @Override
    void build() {
        _nodeVisitor.visit(markdownParser.parse(_content))
    }

    void visitMarkdownNodes(Node node, XWPFParagraph paragraph = null, XWPFRun run = null) {

        if(node instanceof Paragraph) {
            // Initialize a new XWPFParagraph and /XWPFRun each time we descend into a Paragraph node
            paragraph = addParagraph {
                style = Styles.NORMAL
            }
            run = paragraph.createRun()
        }
        else if(node instanceof Text) {

            // Test our assumptions made by the tree traversel algorithum
            assert run != null,
                "PROGRAMMING ERROR! Text node not contained within Paragraph"

            // Append the unescaped text to the run
            run.setText(node.getChars().unescape())
        }
        else {
            //TODO handle stylized spans
        }

        // Descent into child nodes
        node.getChildIterator().each { visitMarkdownNodes(it, paragraph, run) }
    }
}
