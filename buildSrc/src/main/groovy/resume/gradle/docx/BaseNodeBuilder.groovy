
/**
 * 
 */
package resume.gradle.docx

import org.apache.poi.xwpf.usermodel.XWPFDocument
import org.apache.poi.xwpf.usermodel.XWPFParagraph
import org.apache.poi.xwpf.usermodel.XWPFRun
import org.apache.poi.xwpf.usermodel.XWPFTable

import com.vladsch.flexmark.ast.Paragraph
import com.vladsch.flexmark.ast.Text
import com.vladsch.flexmark.ext.gfm.strikethrough.StrikethroughExtension
import com.vladsch.flexmark.ext.tables.TablesExtension
import com.vladsch.flexmark.parser.Parser
import com.vladsch.flexmark.util.ast.Node
import com.vladsch.flexmark.util.ast.NodeVisitor
import com.vladsch.flexmark.util.ast.VisitHandler
import com.vladsch.flexmark.util.data.MutableDataSet

import resume.model.Resume

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
abstract class BaseNodeBuilder {

    protected final XWPFDocument _document

    /**
     * 
     */
    BaseNodeBuilder(XWPFDocument document) {
        _document = document
    }

    XWPFParagraph addParagraph(@DelegatesTo(XWPFParagraph) Closure action) {
        return _document.createParagraph().tap(action)
    }

    XWPFTable addTable(@DelegatesTo(XWPFTable) Closure action) {
        return _document.createTable().tap(action)
    }

    void addMarkdown(final String content) {
        new MarkdownBuilder(_document, content).build()
    }



    abstract void build()
}
