/**
 * 
 */
package resume.gradle.dsl

import resume.model.Accomplishment
import resume.model.JobPosition
import resume.model.ModelFactory

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 * 
 * Used to delegate DSL methods called in the jobPosition closure
 */
@Category(JobPosition)
class JobPositionDelegate {

    static private final ModelFactory _factory = ModelFactory.eINSTANCE

    /**
     * Appends a new Accomplishment instance to the JobPosition
     * @param action
     */
    void acomplishment(@DelegatesTo(Accomplishment) Closure action) {
        this.getAccomplishments().add(_factory.createAccomplishment().tap(action))
    }
}
