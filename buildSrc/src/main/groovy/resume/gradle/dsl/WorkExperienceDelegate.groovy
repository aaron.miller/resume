/**
 * 
 */
package resume.gradle.dsl

import resume.model.JobPosition
import resume.model.ModelFactory
import resume.model.WorkExperience

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 * 
 * Used to delegate DSL methods called in the workExperience closure
 */
@Category(WorkExperience)
class WorkExperienceDelegate {

    static private final ModelFactory _factory = ModelFactory.eINSTANCE

    void position(@DelegatesTo(JobPositionDelegate) Closure action) {
        final JobPosition model =  _factory.createJobPosition()
        use(JobPositionDelegate) {
            model.with(action)
        }

        this.getPositions().add(model)
    }
}
