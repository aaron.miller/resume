/**
 * 
 */
package resume.gradle.dsl

import org.eclipse.emf.ecore.EObject

import resume.model.*

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 * 
 * The ResumeExtension is a builder DSL for constructing fully qualified instances of
 * the Resume model
 */
class ResumeDsl {

    static private final ModelFactory _factory = ModelFactory.eINSTANCE

    // ----------------------------------------------------------
    //
    // Attributes
    //
    // ----------------------------------------------------------

    private final Resume _model = ModelFactory.eINSTANCE.createResume()

    /**
     * Holds a list of referenced instances so they may be added to the root XMI content
     */
    private final List<EObject> _modelRefs = []

    public Resume getModel() {
        return _model
    }

    public List<EObject> getModelRefs() {
        return _modelRefs.unique()
    }

    ContactInfo getContact() {
        return _model.getContact()
    }

    void setContact(ContactInfo value) {
        _model.setContact(value)
    }

    String getTitle() {
        return _model.getTitle()
    }

    void setTitle(String value) {
        _model.setTitle(value)
    }


    // ----------------------------------------------------------
    //
    // Public Methods
    //
    // ----------------------------------------------------------

    /**
     * Construct and return a new ContactInfo instance and store it in the references
     * 
     * @param action
     * @return
     */
    ContactInfo contactInfo(@DelegatesTo(ContactInfo) Closure action) {
        final ContactInfo model = _factory.createContactInfo().tap(action)
        _modelRefs << model
        return model
    }

    /**
     * Retrieve an existing ContactInfo instance by name. An exception is thrown
     * if the instance could not be located
     *
     * @param name
     * @return
     * @throws RuntimeException
     */
    ContactInfo contactInfo(String name) throws RuntimeException {
        return findReferencedModel(ContactInfo, 'name', name)
    }

    /**
     * Constructs a new Calendar instance from a year and month value
     * @param day
     * @param month
     * @param year
     * @return
     */
    Date date(int year, int month) {
        final Calendar cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, month - 1)
        cal.set(Calendar.DAY_OF_MONTH, 1)
        cal.set(Calendar.HOUR, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)

        return cal.getTime()
    }

    /**
     * Configures a CoverLetter instance and assigns it to the model instance
     * 
     * @param action
     */
    void coverLetter(@DelegatesTo(CoverLetter) Closure action) {
        _model.setCoverLetter(_factory.createCoverLetter().tap(action) )
    }

    /**
     * Construct and return a new Employer instance and store it in the references
     * 
     * @param action
     * @return
     */
    Employer employer(@DelegatesTo(Employer) Closure action) {
        final Employer model = _factory.createEmployer().tap(action)
        _modelRefs << model
        return model
    }

    /**
     * Retrieve an existing Employer instance by name. An exception is thrown 
     * if the instance could not be located
     * 
     * @param name
     * @return
     * @throws RuntimeException
     */
    Employer employer(String name) throws RuntimeException {
        return findReferencedModel(Employer, 'name', name)
    }

    /**
     * Configures a Reference instance and assigns it to the model instance
     * 
     * @param action
     */
    void reference(@DelegatesTo(ReferenceDelegate) Closure action) {
        final Reference model =  _factory.createReference()
        use(ReferenceDelegate) {
            model.with(action)
        }

        _model.setReference(model)
    }

    /**
     * Construct and return a new Skill instance and store it in the references
     * 
     * @param action
     * @return
     */
    Skill skill(@DelegatesTo(Skill) Closure action) {
        final Skill model = _factory.createSkill().tap(action)
        _modelRefs << model
        return model
    }

    /**
     * Retrieve an existing Skill instance by name. An exception is thrown
     * if the instance could not be located
     *
     * @param name
     * @return
     * @throws RuntimeException
     */
    Skill skill(String name) throws RuntimeException {
        return findReferencedModel(Skill, 'name', name)
    }

    /**
     * Configures a WorkExperience instance and assigns it to the model instance
     * 
     * @param action
     */
    void workExperience(@DelegatesTo(WorkExperienceDelegate) Closure action) {
        final WorkExperience model =  _factory.createWorkExperience()
        use(WorkExperienceDelegate) {
            model.with(action)
        }

        _model.setWorkExperience(model)
    }

    // ----------------------------------------------------------
    //
    // Private Methods
    //
    // ----------------------------------------------------------


    /**
     * Locates a referenced model instance given a type, key, and value search criteria.
     * An exception is thrown if the instance could not be located
     * 
     * @param <T>
     * @param type
     * @param key
     * @param value
     * @return
     * @throws RuntimeException
     */
    private <T extends EObject> T findReferencedModel(Class<T> type, String key, String value) throws RuntimeException {
        final T model = _modelRefs.find {
            it instanceof T && (it as T).getAt(key) == value
        }

        if(!model) {
            throw new RuntimeException("Could not locate Skill with name=${value}, ensure the instance is created by using the skill method")
        }

        return model
    }
}
