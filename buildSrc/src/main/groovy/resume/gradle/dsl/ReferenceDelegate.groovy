/**
 * 
 */
package resume.gradle.dsl

import resume.model.ModelFactory
import resume.model.Reference

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 * 
 * Used to delegate DSL methods called in the reference closure
 */
@Category(Reference)
class ReferenceDelegate {

    static private final ModelFactory _factory = ModelFactory.eINSTANCE

    void referenceItem(Closure action) {
        this.getItems().add(_factory.createReferenceItem().tap(action))
    }
}
