/**
 * 
 */
package resume.gradle

import java.awt.Color
import java.awt.Dimension

import javax.inject.Inject

import org.gradle.api.DefaultTask
import org.gradle.api.file.ProjectLayout
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

import com.kennycason.kumo.CollisionMode
import com.kennycason.kumo.WordCloud
import com.kennycason.kumo.WordFrequency
import com.kennycason.kumo.bg.RectangleBackground
import com.kennycason.kumo.font.scale.FontScalar
import com.kennycason.kumo.font.scale.LinearFontScalar
import com.kennycason.kumo.palette.ColorPalette

import resume.gradle.dsl.ResumeDsl
import resume.model.Resume
import resume.model.Skill

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 * 
 * TODO all static defaults should have a task input override
 */
class RenderResumeSkillCloud extends DefaultTask {

    static final String DEFAULT_OUTPUT_PATH = 'resume/img/skill-cloud.png'

    static final Integer DEFAULT_IMG_WIDTH = 640

    static final Integer DEFAULT_IMG_HEIGHT = 480

    static final ColorPalette DEFAULT_COLOR_PALETTE = new ColorPalette(new Color(0x182226), new Color(0x418AB7), new Color(0x285059), new Color(0x062B5B))

    static final FontScalar DEFAULT_FONT_SCALAR = new LinearFontScalar(10, 60)
    
    static final Color DEFAULT_BG_COLOR = new Color(0xF2F2F2)
    
    /**
     * Flattens all Skill instances and returns an instance of
     * List<WordFrequency>, containing the counts for each Skill by name
     * 
     * @param resume
     * @return
     */
    static List<WordFrequency> countSkills(final Resume resume) {
        return (resume.workExperience?.positions*.accomplishments*.skills)
                .flatten()
                .countBy { Skill obj -> obj.name }
                .collect { String name, Integer count ->
                    new WordFrequency(name, count)
                }
    }

    static WordCloud constructWordCloud(final List<WordFrequency> wordFrequencies) {

        //Set dimensions of output image
        //TODO use task input if present
        final Dimension dimension = new Dimension(DEFAULT_IMG_WIDTH, DEFAULT_IMG_HEIGHT)

        // Configure a new WordCloud instance
        final WordCloud wordCloud = new WordCloud(dimension, CollisionMode.RECTANGLE)
        wordCloud.setPadding(0)
        wordCloud.setBackground(new RectangleBackground(dimension))
        wordCloud.setBackgroundColor(DEFAULT_BG_COLOR)
        wordCloud.setColorPalette(DEFAULT_COLOR_PALETTE)
        wordCloud.setFontScalar(DEFAULT_FONT_SCALAR)
        wordCloud.build(wordFrequencies)

        return wordCloud
    }

    /**
     * TODO this is a dummy input so that incremental task support
     * will correctly detect changes defined by the ResumeExtension.
     * This should eventually be replaced with an appropriate
     * implementation for ECore model serialization.
     */
    @Input
    Provider<Map> modelProvider

    @Internal
    final Property<ResumeDsl> resume = objectFactory.property(ResumeDsl)

    @OutputFile
    final RegularFileProperty outputFile = objectFactory.fileProperty()

    @Inject
    ObjectFactory getObjectFactory() {}

    @Inject
    ProviderFactory getProviderFactory() {}

    /**
     * 
     */
    @Inject
    RenderResumeSkillCloud(ProjectLayout layout) {
        super()

        //set default output convention
        this.outputFile.convention(layout.buildDirectory.file(DEFAULT_OUTPUT_PATH))
    }


    /**
     *
     */
    @TaskAction
    void run() {

        final Resume resume = this.resume.get().getModel()

        // Build a WordFrequency list from the counted Skill names
        List<WordFrequency> wordFrequencies = countSkills(resume)

        // Create a new WordCloud instance with the default settings
        final WordCloud wordCloud = constructWordCloud(wordFrequencies)

        // Stream the output to the file, ensuring our stream
        // is closed when complete
        final File outputFile = this.outputFile.get().asFile
        outputFile.withOutputStream { OutputStream os ->
            wordCloud.writeToStreamAsPNG(os)
        }

        logger.info("Generated Skill Cloud: ${outputFile}")
    }
}
