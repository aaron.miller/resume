# Resume - Aaron R Miller

This project serves as both a code same and personal resume for Aaron R Miller.
The end resule is a published version of the Resume document in browsable html and a downloadable
word document.

The finished result can be located here: <http://aaron.miller.gitlab.io/resume>

## Getting Started

Running the following commands will check out the project, run a full clean-build, and deploys the build artifacts into the `/public` directory for GitLab pages deployment.

```shell
git clone https://gitlab.com/aaron.miller/resume.git
cd resume
./gradlew.bat deploy
#...or use ./gradlew for posix (needs +x privliges)
```

**Note:** The first buid will take some time as it needs to compile all of the Eclipse dependencies from their OSGI repos and make them available to the build in a maven compatible format. Running this process from scratch with Eclipse open may result in random build failures so running the first build from the command line is recommended. The first project import into an IDE should also be ok, results may vary.


## Features

### Resume Model

The Resume model is defined using the Eclipse Modeling Framework (EMF). This is an editing framework for quickly defining ECore domain models, which can then be used to generate Form Based UI's, data exchange formats, and much more. More info can be found here <https://eclipsesource.com/blogs/tutorials/emf-tutorial/>

![Resume Model Diagram](docs/img/resume.model.jpg)

### Gradle Plugin

The entry point to the code can be found in the [ResumeGradlePlugin](https://gitlab.com/aaron.miller/resume/blob/master/gradle-plugin/src/main/groovy/resume/gradle/ResumeGradlePlugin.groovy). Here you will find all the backend functionality behind the `resume.gradle` plugin applied in the top-level [build.gradle](https://gitlab.com/aaron.miller/resume/blob/master/build.gradle) script. In addition to a builder DSL for constructing the Resume model (more on that later), it defines a few tasks for serializing the model in html, docx, and XMI formats.


### HTML Template

HTML generation is performed by the `resumeHtml` task, a simple gradle task utilizing the [Groovy Template Engine](http://docs.groovy-lang.org/docs/next/html/documentation/template-engines.html#_the_markuptemplateengine) to do the rendering.
The default template engine has been extended to provide automatic markdown conversion of text content.

### Docx Generation

A word document is generated from the model using Apache POI, including support for markdown text content.

### Skill Cloud

A word cloud image is generated from the skills listed in the Resume model. This image is then referenced in the html and docx generator tasks. See the `RenderSkillCloud` task for more information.
