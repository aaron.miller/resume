package template.html

layout 'default-layout.groovy', true,
pageContents: contents {
    div(id: 'index', 'data-role': 'page') {
        div(role: "main", class: "ui-content") {
            div(class: "ui-body ui-body-a ui-corner-all") {
                h3("Introduction")
                div(class: "flex-container") {
                    div(class: "flex-item") {
                        yieldMarkdown """
                        This is an interactive html version of my personal resume. In addition to providing a static 
                        link to my current resume, the project itself serves as a code sample, and may offer some 
                        insights to my workflow during software development.
    
                        The project defines a Resume domain model in a bounded context, and provides a Gradle plugin
                        which provides a builder DSL for authoring the Resume document, and publishing it as a Word
                        document, and browsable html site.
    
                        This project is located at <https://gitlab.com/aaron.miller/resume>
                        """.stripIndent()
                    }
                    div(class: "flex-item") {
                        h4("12 Years of Software Development at a Glance")
                        figure('data-role': 'skillcloud') {
                            img(src: "img/skill-cloud.png", width: 480, height: 360, alt: "Skill Word Cloud")
                        }
                    }
                }
                div(class: "flex-container") {
                    div(class: "flex-item")
                    div(class: "flex-item")
                    a(href: "cover_letter.html",
                    class: "ui-btn ui-btn-inline ui-btn-icon-right ui-icon-carat-r flex-item",
                    'data-transition': "slidefade",
                    "Proceed to Resume")
                }
            }
        }
    }
}
