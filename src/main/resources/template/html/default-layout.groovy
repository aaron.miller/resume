package template.html

yieldUnescaped '<!DOCTYPE html>'
html(lang:'en') {
    head {
        meta('http-equiv':'"Content-Type" content="text/html; charset=utf-8"')
        meta(name: 'viewport', content: "width=device-width, initial-scale=1")
        link(rel: 'stylesheet', href: "lib/jquery-mobile/jquery.mobile-1.4.5.min.css")
        link(rel: "stylesheet", href: "lib/jquery-mobile/jquery.mobile.icons.min.css")
        link(rel: "stylesheet", href: "css/resume.css")
        link(rel: 'stylesheet', href: "css/app.css")
        script(src: "lib/jquery/jquery-2.2.4.min.js")
        script(src: "lib/jquery-mobile/jquery.mobile-1.4.5.min.js")
        title(resume.title)
    }
    body {

        div('data-role': "header") {
            a(href: "#contact",
            'data-rel': 'dialog',
            class: "ui-btn ui-btn-b ui-btn-left ui-btn-inline ui-btn-icon-left ui-icon-mail",
            "Contact")
            h1(resume.title)
            a(href: "aaron.miller-resume.docx", target: "_blank",
            class: "ui-btn ui-btn-b ui-btn-right ui-btn-inline ui-btn-icon-right ui-icon-action",
            "Download")
        }

        pageContents()

        // Contact Info Popup
        ////

        div(id: 'contact',
        'data-role': 'page',
        'data-dialog': true,
        'data-close-btn': "right",
        'data-close-btn-text': "Close") {
            div('data-role': "header") { h1("Contact Info") }
            div(role: "main", class: "ui-content") {
                h3("${resume.contact.name}")
                div(class: "flex-container") {
                    address(class: "flex-item") {
                        yieldMarkdown resume.contact.address
                        yieldUnescaped "<a href='mainto:${resume.contact.email}'>${resume.contact.email}</a><br>"
                        yieldUnescaped resume.contact.phone
                    }
                    img(class: "flex-item", src: "img/avatar.jpg", alt: "Profile Picture")
                }
            }
        }

        script(src: 'js/app.js')
        
        script(src: "https://www.googletagmanager.com/gtag/js?id=UA-153246643-1")
        script {
            yieldUnescaped '''
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-153246643-1');
            '''.stripIndent()
        }

    }
}