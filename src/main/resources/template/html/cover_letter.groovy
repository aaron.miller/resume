package template.html

layout 'default-layout.groovy', true,
pageContents: contents {

    div(id: 'cover_letter', 'data-role': 'page') {
        div(role: "main", class: "ui-content") {
            div(class: "ui-body ui-body-a ui-corner-all") {
                h3(resume.coverLetter.title)
                yieldMarkdown resume.coverLetter.content
                div(class: "flex-container") {
                    a(href: "index.html",
                    class: "ui-btn ui-btn-inline ui-btn-icon-left ui-icon-carat-l flex-item",
                    'data-rel': "back",
                    'data-transition': "slidefade",
                    "Introduction")
                    div(class: "flex-item")
                    a(href: "work_experience.html",
                    class: "ui-btn ui-btn-inline ui-btn-icon-right ui-icon-carat-r flex-item",
                    'data-transition': "slidefade",
                    "Work Experience")
                }
            }
        }
    }
    
}
