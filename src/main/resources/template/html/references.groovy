package template.html

layout 'default-layout.groovy', true,
pageContents: contents {

    div(id: 'references', 'data-role': 'page') {
        div(role: "main", class: "ui-content") {
            div(class: "ui-body ui-body-a ui-corner-all") {
                h3(resume.reference.title)
                resume.reference.items.each { item ->
                    yieldMarkdown item.description
                }
                div(class: "flex-container") {
                    a(href: "work_experience.html",
                    class: "ui-btn ui-btn-inline ui-btn-icon-left ui-icon-carat-l flex-item",
                    'data-rel': "back",
                    'data-transition': "slidefade",
                    "Work Experience")
                    div(class: "flex-item")
                    div(class: "flex-item")
                }
            }
        }
    }
}
