package template.html

String toMonthsString(Date startDate, Date endDate) {
    if(!startDate) { return "0 mo" }
    if(!endDate) { endDate = new Date() }

    final Calendar start = Calendar.getInstance()
    start.setTime(startDate)

    final Calendar end = Calendar.getInstance()
    end.setTime(endDate)

    int diffYear = end.get(Calendar.YEAR) - start.get(Calendar.YEAR)
    int diffMonth = diffYear * 12 + end.get(Calendar.MONTH) - start.get(Calendar.MONTH)

    // Use 1 month if start and end in the same month
    if(diffMonth == 0) {
        return "1 mo"
    }

    if(diffMonth > 12) {
        int diffYears = diffMonth / 12
        diffMonth = diffMonth % 12
        return "${diffYears}yr, ${diffMonth} mo"
    }

    return "${diffMonth} mo"
}

layout 'default-layout.groovy', true,
pageContents: contents {

    div(id: 'work_experience', 'data-role': 'page') {
        div(role: "main", class: "ui-content") {
            div(class: "ui-body ui-body-a ui-corner-all") {
                h3(resume.workExperience.title)

                //sort positions in reverse order by start date
                resume.workExperience?.positions
                       .sort({ a, b -> (b.startDate) <=> (a.startDate)})
                       .each { position ->
                    div('data-role': "collapsible",
                    'data-inset': false,
                    'data-collapsed': true) {
                        h4("${position.title} @ ${position.employer?.name ?: ''} (${toMonthsString(position.startDate, position.endDate)})")
                        yieldMarkdown position.description
                        position.accomplishments?.each { accomp ->
                            div(class: "accomp-item") {
                                h5(accomp.title)
                                yieldMarkdown accomp.description
                                p(class: 'skill-items', accomp.skills.collect({ "<span>${it.name}</span>" }).join(', '))
                            }
                        }
                    }
                }

                div(class: "flex-container") {
                    a(href: "cover_letter.html",
                    class: "ui-btn ui-btn-inline ui-btn-icon-left ui-icon-carat-l flex-item",
                    'data-rel': "back",
                    'data-transition': "slidefade",
                    "Cover Letter")
                    div(class: "flex-item")
                    a(href: "references.html",
                    class: "ui-btn ui-btn-inline ui-btn-icon-right ui-icon-carat-r flex-item",
                    'data-transition': "slidefade",
                    "References")
                }
            }
        }
    }
}
